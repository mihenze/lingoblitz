package com.svarogsoft.lingoblitz.dictionary;

import android.content.Context;

import com.svarogsoft.lingoblitz.card.PartOfSpeech;
import com.svarogsoft.lingoblitz.card.StateCard;
import com.svarogsoft.lingoblitz.data.DataAppHelper;
import com.svarogsoft.lingoblitz.data.DatabaseAdapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**Модель данных для импорта в БД*/

public class ImportModel {
    //контейнер для шаблонов с порядковыми номерами позиций в пользовательском тексте
    Map<String, Integer> templateMap;
    //разделитель между словами
    String separator;
    //пользовательский файл
    File importFile;
    DatabaseAdapter databaseAdapter;
    Context context;

    public ImportModel(Context context){
        //адаптер для работы с БД
        databaseAdapter = new DatabaseAdapter(context);
        this.context = context;
    }
    //устанавливаем файл для импорта
    public void setImportFile(String path) {
        this.importFile = new File(path);
    }
    //устанавливаем разделитель
    public void setSeparator(String separator){
        this.separator = separator;
    }
    //устанавливаем контейнер значений
    public void setTemplateMap(List<String> templates) {
        templateMap = new HashMap<>();
        for(int i = 0; i < templates.size(); i++){
            templateMap.put(templates.get(i), i);
        }
    }
    //метод добавления в базу
    public void addDatabase(){
        //TODO подумать о возвращении значения bool
        //автоматически закроет потоки, если возникнет исключение
        try (InputStream inputStream = new FileInputStream(importFile);
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))){
            //контейнеры слов по типам
            List<String> englishList = new ArrayList<>();
            List<String> russianList = new ArrayList<>();
            List<String> transList = new ArrayList<>();
            List<StateCard> stateList = new ArrayList<>();
            List<PartOfSpeech> partsList = new ArrayList<>();
            List<String> themeList = new ArrayList<>();
            //заполняем контейнеры
            while (reader.ready()){
                //считываем строку
                String line = reader.readLine();
                //парсим ее
                String[] array = line.split(separator);
                //проверяем размер
                if (!(array.length == templateMap.size())) //TODO выбрать верный тип исключения
                    throw new IOException(); //если не соответствует бросаем исключение

                //TODO уйти от констант
                //получаем слова по позиции изучаемого и родного языка
                String studiedWord = array[templateMap.get("english")];
                String nativeWord = array[templateMap.get("russian")];
                //выставлям состояние на гтово к изучению
                StateCard state = StateCard.READY;
                //значения для не обязательных полей по ус=молчанию
                String transcription = null;
                PartOfSpeech parts = PartOfSpeech.NONE;
                String theme = null;
                //выставляем необязательные значения, если есть
                if (templateMap.containsKey("transcription"))
                    transcription = array[templateMap.get("transcription")];
                if (templateMap.containsKey("parts of speech"))
                    parts = PartOfSpeech.valueOf(array[templateMap.get("parts of speech")]);
                if (templateMap.containsKey("theme"))
                    theme = array[templateMap.get("theme")];

                englishList.add(studiedWord);
                russianList.add(nativeWord);
                transList.add(transcription);
                stateList.add(state);
                partsList.add(parts);
                themeList.add(theme);
            }
            //добавляем слова в бд
            databaseAdapter.addWords(englishList, russianList, transList, stateList, partsList, themeList);
            databaseAdapter.deleteDuplicate(); //удаляем дубликаты

            //проверяем темы и добавляем в список
            DataAppHelper dataAppHelper = new DataAppHelper(context);
            dataAppHelper.verifyAddTheme(themeList);
            dataAppHelper.verifyRemovedTheme(databaseAdapter);

        } catch (IOException e) {
            throw new Error("ErrorAddDataBD"); //ошибка добавления в БД
        }
    }
}
