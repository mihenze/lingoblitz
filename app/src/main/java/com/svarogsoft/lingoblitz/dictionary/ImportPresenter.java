package com.svarogsoft.lingoblitz.dictionary;

import android.content.Context;

import com.svarogsoft.lingoblitz.R;

import java.util.List;

/**Посредник между моделью и вьювером для словаря*/
public class ImportPresenter {

    private IImportView importView;
    private ImportModel importModel;

    public ImportPresenter(IImportView importView, Context context){
        this.importView = importView;
        importModel = new ImportModel(context);
    }

    //импорт данных
    public void isImport(){
        //получим данные от представления
        //TODO наверное эти действия тоже стоит поручить модели данных, так как посреднику о них знать незачем прочитать еще о MVP
        //разделитель
        String separator = importView.getSeparator();
        if(separator == null || separator.equals("")){
            //уведомим об этом пользователя
            importView.showToast(R.string.toast_notice_import_separator);
            return;
        }
        //шаблон
        List<String> templates = importView.getTemplate();
        if(templates == null || templates.size() == 0){
            //уведомим об этом пользователя
            importView.showToast(R.string.toast_notice_import_empty);
            return;
        }
        //TODO уйти от констант!!!
        if(!templates.contains("russian") || !templates.contains("english")){
            //уведомим об этом пользователя
            importView.showToast(R.string.toast_notice_import_template);
            return;
        }

        //продолжаем работу если все хорошо
        //модель для работы с данными
        importModel.setSeparator(separator);
        importModel.setTemplateMap(templates);

        //запрашиваем открытие файла
        importView.startFileDialog();



    }

    public void isFileOpen() {
        //запрашиваем путь у активности до файла
        String path = importView.getPathImportFile();
        if(path == null){
            //уведомим об этом пользователя
            importView.showToast(R.string.toast_notice_import_error);
            return;
        }
        importModel.setImportFile(path);
        importModel.addDatabase();
        //уведомим об этом пользователя
        importView.showToast(R.string.toast_notice_import_success);

    }
}
