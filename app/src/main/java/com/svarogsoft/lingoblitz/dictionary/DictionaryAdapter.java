package com.svarogsoft.lingoblitz.dictionary;

import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.svarogsoft.lingoblitz.R;
import com.svarogsoft.lingoblitz.data.DatabaseHelper;

/**Отображение и действия над карточкой в словаре*/

public class DictionaryAdapter extends CursorRecyclerAdapter<DictionaryAdapter.DictionaryViewHolder> {
    //слушатель клика по карте
    private final OnWordClickListener onWordClickListener;

    public DictionaryAdapter(Cursor cursor, OnWordClickListener onWordClickListener){
        super(cursor);
        this.onWordClickListener = onWordClickListener;
    }
    //выполняет привязку объекта DictionaryViewHolder к объекту Карта слова по доступным позициям
    @Override
    public void onBindViewHolder(DictionaryViewHolder viewHolder, Cursor cursor) {
        long id = cursor.getLong(idColumnIndex);
        viewHolder.itemView.setTag(id);
        viewHolder.english.setText(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_ENGLISH)));
        viewHolder.russian.setText(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_RUSSIAN)));
        viewHolder.transkription.setText(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_TRANSCRIPTION)));
        viewHolder.state.setText(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_STATE)));
        viewHolder.parts.setText(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_PARTSOFSPEECH)));
        viewHolder.theme.setText(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_THEME)));
    }

    //возвращает объект DictionaryViewHolder который будет хранить данные по одному объекту - Карта слова
    @Override
    public DictionaryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.decorate_card_dictionary, parent,false);
        return new DictionaryViewHolder(view);
    }


    class DictionaryViewHolder extends RecyclerView.ViewHolder {

        private final TextView english;
        private final TextView russian;
        private final TextView transkription;
        private final TextView state;
        private final TextView parts;
        private final TextView theme;

        public DictionaryViewHolder(View itemView) {
            super(itemView);

            this.english = itemView.findViewById(R.id.english_word);
            this.russian = itemView.findViewById(R.id.russian_word);
            this.transkription = itemView.findViewById(R.id.transk_word);
            this.state = itemView.findViewById(R.id.state_word);
            this.parts = itemView.findViewById(R.id.parts_word);
            this.theme = itemView.findViewById(R.id.theme_word);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    long wordId = (long) v.getTag();
                    onWordClickListener.onWordClick(wordId);
                }
            });
        }
    }

    /**
     * Слушатель для обработки кликов
     */
    public interface OnWordClickListener{
        void onWordClick(long wordId);
    }
}
