package com.svarogsoft.lingoblitz.dictionary;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.svarogsoft.lingoblitz.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**Адаптер(представление) для файлов и директорий при работе с файловым менеджером
 * свои пиктограммы и обработчик клика*/
/**
 * Класс адаптера наследуется от RecyclerView.Adapter с указанием класса, который будет хранить ссылки на виджеты элемента списка,
 * т.е. класса, имплементирующего ViewHolder. В нашем случае класс объявлен внутри класса адаптера.
 */
public class FilesAdapter extends RecyclerView.Adapter<FilesAdapter.FilesViewHolder> {
    //Константы типов
    private static final int TYPE_DIRECTORY = 0; //директория
    private static final int TYPE_FILE_KNOWN = 1; //известный файл, который можно прочитать
    private static final int TYPE_FILE_UNKNOWN = 2; //неизвестный файл, который не прочитать

    private List<File> files = new ArrayList<>();

    @Nullable //говорит о том, что поле вероятно может быть null, и чтобы избежать неожиданностей возможно стоит его проверять
    private OnFileClickListener onFileClickListener;

    public void setOnFileClickListener(@Nullable OnFileClickListener onFileClickListener) {
        this.onFileClickListener = onFileClickListener;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }

    //определение типа элемента - директория или файл
    @Override
    public int getItemViewType(int position) {
        File file = files.get(position);
        if (file.isDirectory()) {
            return TYPE_DIRECTORY;
        } else {
            if (file.getName().endsWith(".txt")){ //TODO а если несколько файлов подумать о сравнении
                return TYPE_FILE_KNOWN;
            }
            return TYPE_FILE_UNKNOWN;
        }
    }
    //создает(когда это требуется) и возвращает объект FilesViewHolder который будет хранить данные по одному объекту - Файл
    /**
     * Создание новых View и ViewHolder элемента списка, которые впоследствии могут переиспользоваться.
     */
    @Override
    public FilesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        //выставляем соответствующие иконки по типу файла
        View view;
        if (viewType == TYPE_DIRECTORY) {
            view = layoutInflater.inflate(R.layout.decorate_item_directory, parent, false);
        } else if (viewType == TYPE_FILE_KNOWN) {
            view = layoutInflater.inflate(R.layout.decorate_item_file_known, parent, false);
        } else {
            view = layoutInflater.inflate(R.layout.decorate_item_file_unknown, parent, false);
        }
        return new FilesViewHolder(view);
    }
    //выполняет привязку объекта FilesViewHolder к объекту Файл по доступным позициям и устанавливает данные во view
    /**
     * Заполнение виджетов View данными из элемента списка с номером i
     */
    @Override
    public void onBindViewHolder(FilesViewHolder holder, int position) {
        File file = files.get(position);
        holder.captionFile.setText(file.getName());
        holder.itemView.setTag(file); //для ассоциации элемента списка с конкретным файлом
    }
    //возвращает количество объектов в списке
    @Override
    public int getItemCount() {
        return files.size();
    }

    /*Обязательное использование паттерна ViewHolder.
    объекты ViewHolder создаются для нескольких первых view-компонентов,
    а затем они переиспользуются, и адаптер просто привязывает данные при помощи метода onBindViewHolder()*/
    /**
            * Реализация класса ViewHolder, хранящего ссылки на виджеты.
            */
    class FilesViewHolder extends RecyclerView.ViewHolder {
        private final TextView captionFile;

        public FilesViewHolder(View view){
            super(view);
            this.captionFile = view.findViewById(R.id.caption_file);
            //слушатель для клика по файлу илид иректории
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    File file = (File) view.getTag();
                    if (onFileClickListener != null) {
                        onFileClickListener.onFileClick(file); //уведомление наблюдателя о событии
                    }
                }
            });
        }
    }

    public interface OnFileClickListener {
        void onFileClick(File file);
    }
}
