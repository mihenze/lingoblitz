package com.svarogsoft.lingoblitz.dictionary;

import android.database.Cursor;
import android.widget.Filter;

/**CursorFilter делегирует большую часть работы CursorAdapter.*/
public class CursorFilter extends Filter {

    CursorFilterClient client;

    interface CursorFilterClient{
        CharSequence convertToString(Cursor cursor);
        Cursor runQueryOnBackgroundThread(CharSequence constraint);
        Cursor getCursor();
        void changeCursor(Cursor cursor);
    }

    CursorFilter(CursorFilterClient client){
        this.client = client;
    }

    //Преобразует значение из отфильтрованного набора в последовательность CharSequence
    @Override
    public CharSequence convertResultToString(Object resultValue){
        return client.convertToString((Cursor) resultValue);
    }
    //Вызывается в рабочем потоке для фильтрации данных в соответствии с ограничением. Подклассы должны реализовать этот метод для выполнения операции фильтрации.
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        Cursor cursor = client.runQueryOnBackgroundThread(constraint);
        FilterResults results = new FilterResults();
        if(cursor != null){
            results.count = cursor.getCount();
            results.values = cursor;
        } else {
            results.count = 0;
            results.values = null;
        }
        return results;
    }
    //Вызывается в потоке пользовательского интерфейса для публикации результатов фильтрации в пользовательском интерфейсе.
    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        Cursor oldCursor = client.getCursor();

        if(results.values != null && results.values != oldCursor){
            client.changeCursor((Cursor) results.values);
        }
    }
}
