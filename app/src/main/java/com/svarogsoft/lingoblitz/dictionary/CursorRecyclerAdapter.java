package com.svarogsoft.lingoblitz.dictionary;

import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;

import android.os.Handler;
import android.widget.Filter;
import android.widget.FilterQueryProvider;
import android.widget.Filterable;


import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.svarogsoft.lingoblitz.data.DatabaseHelper;

//TODO разобраться с этим классом!!!!! Упростить раболту с БД - посмотреть в сторону LoaderManager

/**Абстрактный класс адаптера RecyclerView с курсором*/

public abstract class CursorRecyclerAdapter<ViewHolder extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<ViewHolder> implements Filterable, CursorFilter.CursorFilterClient  {
    protected Cursor cursor; //курсор
    protected boolean isDataValid; //валидны ли данные
    protected int idColumnIndex; //индекс столбца ID в курсоре
    //наблюдатели
    private ChangeObserver changeObserver; //за контентом
    private DataSetObserver dataSetObserver; //за набором данных Cursor

    private CursorFilter cursorFilter; //фильтр
    private FilterQueryProvider filterQueryProvider; //провайдер фильтрации исп. для определения способа фильтрации

    public CursorRecyclerAdapter(Cursor cursor){
        super();
        this.cursor = cursor;
        isDataValid = cursor != null; //данные валидны если курсор не null
        idColumnIndex = cursor != null ? cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_ID) : -1; //получаем индекс столбца ID
        setHasStableIds(true);//каждый элемент имеет уникальный id

        changeObserver = new ChangeObserver();
        dataSetObserver = new MyDataSetObserver();

        if(isDataValid){ //регистрация наблюдателей
            if(changeObserver != null) cursor.registerContentObserver(changeObserver);
            if(dataSetObserver != null) cursor.registerDataSetObserver(dataSetObserver);
        }
    }

    //перемещает курсор в нужную позицию и вызывает новый абстрактный метод, который должен быть определен в классе неследнике
    //нужен для определения позиции и курсора БД
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //Если данные не корректны кидаем исключение
        if (!isDataValid){
            throw new IllegalStateException("Cursor is not valid!");
        }
        //Попробовали перейти к определенной строке но это не получилось
        if(!cursor.moveToPosition(position)){
            throw new IllegalStateException("Can not move to position" + position);
        }
        //вызываем новый метод
        onBindViewHolder(holder, cursor);
    }
    /*метод для переопределния в классе наследнике для привязки holder'a к объекту*/
    public abstract void onBindViewHolder(ViewHolder viewHolder, Cursor cursor);

    @Override
    public long getItemId(int position) {
        //если с данными все хорошо и есть курсор
        if(isDataValid && cursor != null){
            //если смогли найти нужную строку в курсоре
            if (cursor.moveToPosition(position))
                //возвращаем значение столбца ID
                return cursor.getLong(idColumnIndex);
        }
        //во всех остальных случаях возврощаем дефолтное значение
        return RecyclerView.NO_ID;
    }

    @Override
    public int getItemCount() {
        if(isDataValid && cursor != null)
            return cursor.getCount();
        else
            return 0;
    }

    public Cursor getCursor(){
        return cursor;
    }
    //замена курсора, если он был то старый будет закрыт
    @Override
    public void changeCursor(Cursor cursor) {
        Cursor oldCursor = swapCursor(cursor);
        if(oldCursor != null) oldCursor.close();
    }

    //Заменяет старый курсор новым
    @Nullable
    public Cursor swapCursor(Cursor newCursor){
        // Если курсор не изменился — ничего не заменяем
        if (newCursor == this.cursor) {
            return null;
        }
        //старый курсор
        Cursor oldCursor = this.cursor;
        if(oldCursor != null){ //если он был, то снимаем регистрацию
            if(changeObserver != null) oldCursor.unregisterContentObserver(changeObserver);
            if(dataSetObserver != null) oldCursor.unregisterDataSetObserver(dataSetObserver);
        }
        //новый курсор
        this.cursor = newCursor;
        //если он есть регистрируем, обновляем значения
        if(newCursor != null){
            if(changeObserver != null) newCursor.registerContentObserver(changeObserver);
            if(dataSetObserver != null) newCursor.registerDataSetObserver(dataSetObserver);

            idColumnIndex = newCursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_ID);
            isDataValid = true;
            notifyDataSetChanged(); //сообщаем адаптеру что данные изменились и их нужно перерисовать
        } else {
            idColumnIndex = -1;
            isDataValid = false;
            //сообщаем что данных в адаптере больше нет
            notifyItemRangeRemoved(0, getItemCount());
        }
        //возвращаем старый курсор
        return oldCursor;
    }
    //возвращаем строковое представление по умолчанию (подклассы должны его переопределять)
    @Override
    public CharSequence convertToString(Cursor cursor) {
        return cursor == null ? "" : cursor.toString();
    }
    //выполняет запрос с указанным ограничением, данный запрос запрашивается фильтром, прикрепленным к этому адаптеру
    @Override
    public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
        if(filterQueryProvider != null){
            return filterQueryProvider.runQuery(constraint);
        }
        return this.cursor;
    }

    @Override
    public Filter getFilter() {
        if(cursorFilter == null){
            cursorFilter = new CursorFilter(this);
        }
        return cursorFilter;
    }
    //возвращает провайдер фильтрации
    public FilterQueryProvider getFilterQueryProvider() {
        return filterQueryProvider;
    }
    //установка провайдера фильтрации
    public void setFilterQueryProvider(FilterQueryProvider filterQueryProvider) {
        this.filterQueryProvider = filterQueryProvider;
    }
    //вызывается когда наблюдатель ContentObserver на курсоре получает уведомлении об изменении, может быть реализовано подклассом
    protected void onContentChanged(){

    }

    private class ChangeObserver extends ContentObserver{


        public ChangeObserver() {
            super(new Handler());
        }

        @Override
        public boolean deliverSelfNotifications() {
            return true;
        }

        @Override
        public void onChange(boolean selfChange) {
            onContentChanged();
        }
    }

    private class MyDataSetObserver extends DataSetObserver {
        @Override
        public void onChanged() {
            isDataValid = true;
            notifyDataSetChanged();
        }

        @Override
        public void onInvalidated() {
            isDataValid = false;
            notifyItemRangeRemoved(0, getItemCount());
        }
    }

}
