package com.svarogsoft.lingoblitz.dictionary;

import java.util.List;

public interface IImportView {
    List<String> getTemplate();
    String getSeparator();
    void showToast(int message);
    void startFileDialog();
    String getPathImportFile();
}
