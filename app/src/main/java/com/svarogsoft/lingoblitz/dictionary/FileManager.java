package com.svarogsoft.lingoblitz.dictionary;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**Простейший файловый менеджер с переходами в глубь катологов и обратно с проверками*/

public class FileManager {
    private File currentDirectory; //текущая директория
    private boolean isRoot; //флаг корневого каталога
    private List<File> rootDirectories; //директория выше которой подниматься нельзя

    public FileManager(Context context){

        //при создании выставляем флаг корневого каталога в true
        isRoot = true;
        //инициализируем корневые директории
        initRootDirectories(context);
    }

    private void initRootDirectories(Context context){
        //получаем пути
        rootDirectories = new ArrayList<>();
        File storageDir = Environment.getExternalStorageDirectory();
        String storagePath = storageDir.getAbsolutePath();
        //получаем список каталогов
        File[] extFileDirs = context.getExternalFilesDirs(null);
        String suffixToStrip = null;
        //вычленяем нужный путь
        for (File nextFile : extFileDirs) {
            String nextPath = nextFile.getAbsolutePath();
            if (nextPath.startsWith(storagePath)) {
                suffixToStrip = nextPath.substring(storagePath.length());
                break;
            }
        }
        //перезаписываем каталоги
        if (suffixToStrip != null) {
            for (File nextFile : extFileDirs) {
                String nextPath = nextFile.getAbsolutePath();
                int stripIndex = nextPath.lastIndexOf(suffixToStrip);
                if (stripIndex > -1) {
                    File path = new File(nextPath.substring(0, stripIndex));
                    rootDirectories.add(path);
                }
            }
        }
        //если таких каталогов нет, то берем просто каталог внутренней памяти
        //?? разве такое вообще может быть
        if (rootDirectories.size() == 0) {
            rootDirectories.add(storageDir);
        }
    }
    //в глубь каталогов
    public boolean navigateTo(File directory){
        if(!directory.isDirectory()){ //если не директория
            return false;
        }
        if(isRoot) //снимаем флаг директории рут
            isRoot = false;
        currentDirectory = directory; //устанавливаем текущую директорию
        return true;
    }

    //при нажатии назад
    public boolean navigateUp(){
        if (currentDirectory == null) return false;
        //проверка если текущая директория есть в директориях рут
        if(rootDirectories.contains(currentDirectory)){
            if(!isRoot) { //если мы еще не в рут, то разрешаем переместиться
                isRoot = true; //выставляем флаг корневой директории
                return true;
            } else { //иначе выше нельзя и выходим из окна выбора файла
                return false;
            }
        }
        //иначе работаем как с дочерней директорией
        return navigateTo(currentDirectory.getParentFile());
    }

    //список файлов в текущей директории
    public List<File> getFiles() {
        if (isRoot){ //если в рут директории
            return rootDirectories; //то показываем рут каталоги - внешней памяти и sd
        }
        List<File> files = new ArrayList<>();
        files.addAll(Arrays.asList(currentDirectory.listFiles()));
        return files;
    }
}
