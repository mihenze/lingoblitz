package com.svarogsoft.lingoblitz.data;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

//Класс для работы с БД
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "testBd.db"; // название бд
    private static final int DATABASE_VERSION = 1; // версия базы данных
    private static String DATABASE_PATH; // полный путь к базе данных

    static final String TABLE_WORDS = "WordsTable"; // название таблицы слов в бд

    private final Context myContext;
    private SQLiteDatabase myDataBase;
    private boolean isNeedUpdating = false;

    //названия столбцов
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_ENGLISH = "_englishWord";
    public static final String COLUMN_RUSSIAN = "_russianWord";
    public static final String COLUMN_TRANSCRIPTION = "_transcription";
    public static final String COLUMN_STATE = "_state";
    public static final String COLUMN_COUNT = "_count";
    public static final String COLUMN_PARTSOFSPEECH = "_partsOfSpeech";
    public static final String COLUMN_THEME = "_theme";

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.myContext = context;
        DATABASE_PATH = "/data/data/" + context.getPackageName() + "/databases/" + DATABASE_NAME;
        copyDataBase();
        this.getReadableDatabase();
    }

    public void updateDataBase() throws IOException {
        if (isNeedUpdating) {
            File dbFile = new File(DATABASE_PATH);
            if(dbFile.exists())
                dbFile.delete();
            copyDataBase();
            isNeedUpdating = false;
        }
    }

    private boolean checkDataBase(){
        File dbFile = new File(DATABASE_PATH);
        return dbFile.exists();
    }

    private void copyDataBase() {
        if(!checkDataBase()) {
            this.getReadableDatabase();
            this.close();
            try {
                copyDataBaseFile();
            } catch (IOException e) {
                throw new Error("ErrorCopyingDataBase"); //ошибка копирования БД на устройство
            }
        }
    }

    private void copyDataBaseFile() throws IOException{
        //получаем локальную бд как поток
        InputStream inputStream = myContext.getAssets().open(DATABASE_NAME);
        // Открываем пустую бд
        OutputStream outputStream = new FileOutputStream(DATABASE_PATH);
        // побайтово копируем данные
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) > 0) {
            outputStream.write(buffer, 0, length);
        }

        outputStream.flush();
        outputStream.close();
        inputStream.close();
    }

    public boolean openDataBase() throws SQLException {
        myDataBase = SQLiteDatabase.openDatabase(DATABASE_PATH, null, SQLiteDatabase.CREATE_IF_NECESSARY);
        return myDataBase != null;
    }

    @Override
    public synchronized void close(){
        if(myDataBase != null)
            myDataBase.close();
        super.close();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        //TODO нужен ли? например, когда осутствует БД
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion)
            isNeedUpdating = true;
    }

}
