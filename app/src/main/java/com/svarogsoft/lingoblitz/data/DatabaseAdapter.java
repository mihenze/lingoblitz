package com.svarogsoft.lingoblitz.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.svarogsoft.lingoblitz.card.PartOfSpeech;
import com.svarogsoft.lingoblitz.card.StateCard;
import com.svarogsoft.lingoblitz.card.Card;
import com.svarogsoft.lingoblitz.view.LBApplication;
import com.svarogsoft.lingoblitz.view.SettingsActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DatabaseAdapter {

    private DatabaseHelper databaseHelper;
    private SQLiteDatabase database;

    public DatabaseAdapter(Context context){
        databaseHelper = new DatabaseHelper(context); //.getApplicationContext()
        try {
            databaseHelper.updateDataBase();
        } catch (IOException e) {
            throw new Error("UnableToUpdateDatabase"); //Ошибка - не возможно обновить БД
        }

        try {
            database = databaseHelper.getWritableDatabase();
        } catch (SQLException sqlException) {
            throw sqlException;
        }
    }

    //получаем переданное количестов карточек в режиме всех слов
    public List<Card> getCardsModeAll(int countCards, boolean isFirstRequest){
        List<Card> result = new ArrayList<>(); //временный контейнер для записи слов
        if (isFirstRequest){

            //сначала берем слова из уже изучавшихся
            //просто - Активные
            List<Card> temp = getCardsModeAllState(countCards, StateCard.ACTIVELY);
            int tempSize = countCards - temp.size();
            result.addAll(temp);
            temp.clear();
            //если еще есть место надо добрать новых слов
            if (tempSize > 0)
            {
                temp = getCardsModeAllState(tempSize, StateCard.READY);
                //пометим активными
                updateStateCards(temp, StateCard.ACTIVELY, StateCard.READY);

                result.addAll(temp);
                temp.clear();
            }
        }
        else {
            result.addAll(getCardsModeAllState(countCards, StateCard.READY));
            //пометим активными
            updateStateCards(result, StateCard.ACTIVELY, StateCard.READY);
        }

        return result;
    }

    private List<Card> getCardsModeAllState(int countCards, StateCard columnState){
        List<Card> temp = new ArrayList<>(); //временный контейнер для записи слов
        //TODO говорят, что данный запрос не эффективен и при большом количестве строк, может надолго повесить работу приложения
        //TODO изучить этот вопрос
        String query = String.format("SELECT * FROM %s WHERE %s='%s' ORDER BY RANDOM() LIMIT %d",
                DatabaseHelper.TABLE_WORDS, DatabaseHelper.COLUMN_STATE, columnState.toString(), countCards);

        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID));
                String english = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_ENGLISH));
                String russian = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_RUSSIAN));
                String transcription = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_TRANSCRIPTION));
                StateCard state = StateCard.valueOf(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_STATE)));
                int count = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_COUNT));

                String partsS = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PARTSOFSPEECH));
                PartOfSpeech partsofspeech = PartOfSpeech.NONE;
                if (partsS!=null) partsofspeech = PartOfSpeech.valueOf(partsS.toUpperCase());

                String theme = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_THEME));

                temp.add(new Card(id, english, russian, state, count, transcription, partsofspeech, theme));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return temp;
    }

    //получаем переданное количество карточек в режимы частей речи, в соответсвии с переданной частью речи
    public List<Card> getCardsModeParts(int countCards, PartOfSpeech parts, boolean isFirstRequest){
        List<Card> result = new ArrayList<>(); //временный контейнер для записи слов
        if (isFirstRequest){

            //сначала берем слова из уже изучавшихся
            //просто - Активные
            List<Card> temp = getCardsModePartsState(countCards, StateCard.ACTIVELY, parts);
            int tempSize = countCards - temp.size();
            result.addAll(temp);
            temp.clear();
            //если еще есть место надо добрать новых слов
            if (tempSize > 0)
            {
                temp = getCardsModePartsState(tempSize, StateCard.READY, parts);
                //пометим активными
                updateStateCards(temp, StateCard.ACTIVELY, StateCard.READY);

                result.addAll(temp);
                temp.clear();
            }
        }
        else {
            result.addAll(getCardsModePartsState(countCards, StateCard.READY, parts));
            //пометим активными
            updateStateCards(result, StateCard.ACTIVELY, StateCard.READY);
        }
        return result;
    }

    private List<Card> getCardsModePartsState(int countCards, StateCard columnState, PartOfSpeech parts){
        List<Card> temp = new ArrayList<>(); //временный контейнер для записи слов
        String query = String.format("SELECT * FROM %s WHERE %s='%s' AND %s='%s' ORDER BY RANDOM() LIMIT %d",
                DatabaseHelper.TABLE_WORDS, DatabaseHelper.COLUMN_STATE, columnState.toString(),
                DatabaseHelper.COLUMN_PARTSOFSPEECH, parts.toString(), countCards);

        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID));
                String english = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_ENGLISH));
                String russian = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_RUSSIAN));
                String transcription = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_TRANSCRIPTION));
                StateCard state = StateCard.valueOf(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_STATE)));
                int count = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_COUNT));

                String partsS = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PARTSOFSPEECH));
                PartOfSpeech partsofspeech = PartOfSpeech.NONE;
                if (partsS!=null) partsofspeech = PartOfSpeech.valueOf(partsS.toUpperCase());

                String theme = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_THEME));

                temp.add(new Card(id, english, russian, state, count, transcription, partsofspeech, theme));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return temp;
    }

    //получаем переданное количество карточек в режимы тем, в соответсвии с переданной частью речи
    public List<Card> getCardsModeTheme(int countCards, String theme, boolean isFirstRequest){
        List<Card> result = new ArrayList<>(); //временный контейнер для записи слов
        if (isFirstRequest){

            //сначала берем слова из уже изучавшихся
            //просто - Активные
            List<Card> temp = getCardsModeThemeState(countCards, StateCard.ACTIVELY, theme);
            int tempSize = countCards - temp.size();
            result.addAll(temp);
            temp.clear();
            //если еще есть место надо добрать новых слов
            if (tempSize > 0)
            {
                temp = getCardsModeThemeState(tempSize, StateCard.READY, theme);
                //пометим активными
                updateStateCards(temp, StateCard.ACTIVELY, StateCard.READY);

                result.addAll(temp);
                temp.clear();
            }
        }
        else {
            result.addAll(getCardsModeThemeState(countCards, StateCard.READY, theme));
            //пометим активными
            updateStateCards(result, StateCard.ACTIVELY, StateCard.READY);
        }
        return result;
    }

    private List<Card> getCardsModeThemeState(int countCards, StateCard columnState, String themeMode){
        List<Card> temp = new ArrayList<>(); //временный контейнер для записи слов
        String query = String.format("SELECT * FROM %s WHERE %s='%s' AND %s LIKE '%%%s%%' ORDER BY RANDOM() LIMIT %d",
                DatabaseHelper.TABLE_WORDS, DatabaseHelper.COLUMN_STATE, columnState.toString(),
                DatabaseHelper.COLUMN_THEME, themeMode, countCards);

        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID));
                String english = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_ENGLISH));
                String russian = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_RUSSIAN));
                String transcription = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_TRANSCRIPTION));
                StateCard state = StateCard.valueOf(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_STATE)));
                int count = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_COUNT));

                String partsS = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PARTSOFSPEECH));
                PartOfSpeech partsofspeech = PartOfSpeech.NONE;
                if (partsS!=null) partsofspeech = PartOfSpeech.valueOf(partsS.toUpperCase());

                String theme = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_THEME));

                temp.add(new Card(id, english, russian, state, count, transcription, partsofspeech, theme));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return temp;
    }

    //изменение статуса выбранных карт с готовности на активное
    public void updateStateCards(List<Card> cards, StateCard stateNow, StateCard stateOld){
        for(Card card : cards){
            if(card.getState() == stateOld){
                card.setState(stateNow);
                String whereClause = DatabaseHelper.COLUMN_ID + "=" + card.getId(); //УСЛОВИЕ WHERE если id в бд = id нашей карты
                ContentValues cv = new ContentValues();
                cv.put(DatabaseHelper.COLUMN_STATE, card.getState().toString()); //статус
                database.update(DatabaseHelper.TABLE_WORDS, cv, whereClause, null); //обновляем
            }
        }
    }

    //Обновление счетчика всех карточек из выбранной стопки
    public void updateCountCards(List<Card> cards){
        for(Card card : cards){
            String whereClause = DatabaseHelper.COLUMN_ID + "=" + card.getId(); //УСЛОВИЕ WHERE если id в бд = id нашей карты
            ContentValues cv = new ContentValues();
            cv.put(DatabaseHelper.COLUMN_COUNT, card.getCount()); //счетчик
            database.update(DatabaseHelper.TABLE_WORDS, cv, whereClause, null); //обновляем
        }
    }

    //Обновление счетчика всех карточек по указанному состоянию
    public void updateCountCardsByState(StateCard state, int count){
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.COLUMN_COUNT, count);
        String whereClause = DatabaseHelper.COLUMN_STATE + " LIKE '" + state.toString()+"'";
        database.update(DatabaseHelper.TABLE_WORDS, cv, whereClause, null);
        /*String updateQuery = String.format("UPDATE %s SET %s=%d WHERE %s LIKE %s",
                DatabaseHelper.TABLE_WORDS, DatabaseHelper.COLUMN_COUNT, count, DatabaseHelper.COLUMN_STATE, state.toString());
        database.execSQL(updateQuery);*/
    }

    public void updateCard(Card card){
        String whereClause = DatabaseHelper.COLUMN_ID + "=" + card.getId(); //УСЛОВИЕ WHERE если id в бд = id нашей карты
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.COLUMN_ENGLISH, card.getStudiedWord()); //слово по английски
        cv.put(DatabaseHelper.COLUMN_RUSSIAN, card.getNativeWord()); //слово по русски
        cv.put(DatabaseHelper.COLUMN_TRANSCRIPTION, card.getTranscription()); //транскрипция
        cv.put(DatabaseHelper.COLUMN_STATE, card.getState().toString()); //статус
        cv.put(DatabaseHelper.COLUMN_COUNT, card.getCount()); //счетчик
        cv.put(DatabaseHelper.COLUMN_PARTSOFSPEECH, card.getPartOfSpeech().toString()); //части речи
        cv.put(DatabaseHelper.COLUMN_THEME, card.getTheme()); //темы
        database.update(DatabaseHelper.TABLE_WORDS, cv, whereClause, null); //обновляем
    }


    /***********************************************************************/

    public boolean isThemeAlive(String theme){
        String query = String.format("SELECT * FROM %s WHERE %s LIKE '%%%s%%'",
                DatabaseHelper.TABLE_WORDS, DatabaseHelper.COLUMN_THEME, theme);

        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            cursor.close();
            return true;
        }
        else {
            cursor.close();
            return false;
        }
    }

    public Card getCard(long id){
        Card card = null;
        String query = String.format("SELECT * FROM %s WHERE %s=%d",
                DatabaseHelper.TABLE_WORDS, DatabaseHelper.COLUMN_ID, id);
        Cursor cursor = database.rawQuery(query, null);
        if (cursor.moveToFirst()) {

            String english = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_ENGLISH));
            String russian = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_RUSSIAN));
            String transcription = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_TRANSCRIPTION));
            StateCard state = StateCard.valueOf(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_STATE)));
            int count = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_COUNT));

            String partsS = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PARTSOFSPEECH));
            PartOfSpeech partsofspeech = PartOfSpeech.NONE;
            if (partsS!=null) partsofspeech = PartOfSpeech.valueOf(partsS.toUpperCase());

            String theme = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_THEME));
            card = new Card(id, english, russian, state, count, transcription, partsofspeech, theme);
        }
        cursor.close();
        return card;
    }

    public void addWord(String english, String russian, String transcription, StateCard state, PartOfSpeech parts, String theme){
        //INSERT INTO sections VALUES (2, 'Digital Systems');
        /*String insertQuery = String.format("INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s) VALUES('%s', '%s', '%s', '%s', %d, '%s', '%s')",
                DatabaseHelper.TABLE_WORDS, DatabaseHelper.COLUMN_ENGLISH, DatabaseHelper.COLUMN_RUSSIAN, DatabaseHelper.COLUMN_TRANSCRIPTION,
                DatabaseHelper.COLUMN_STATE, DatabaseHelper.COLUMN_COUNT, DatabaseHelper.COLUMN_PARTSOFSPEECH, DatabaseHelper.COLUMN_THEME,
                english, russian, transcription, state.toString(), 2, parts.toString(), theme);
        database.execSQL(insertQuery);*/

        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.COLUMN_ENGLISH, english); //слово по английски
        cv.put(DatabaseHelper.COLUMN_RUSSIAN, russian); //слово по русски
        cv.put(DatabaseHelper.COLUMN_TRANSCRIPTION, transcription); //транскрипция
        cv.put(DatabaseHelper.COLUMN_STATE, state.toString()); //статус
        cv.put(DatabaseHelper.COLUMN_COUNT, LBApplication.PREF_COUNT_REPEAT); //счетчик 2
        cv.put(DatabaseHelper.COLUMN_PARTSOFSPEECH, parts.toString()); //части речи
        cv.put(DatabaseHelper.COLUMN_THEME, theme); //темы
        database.insert(DatabaseHelper.TABLE_WORDS, null, cv);
    }

    public void addWords(List<String> english, List<String> russian, List<String> transcription, List<StateCard> state, List<PartOfSpeech> parts, List<String> theme){
        String insertQuery = String.format("INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s) VALUES(?, ?, ?, ?, ?, ?, ?)",
                DatabaseHelper.TABLE_WORDS, DatabaseHelper.COLUMN_ENGLISH, DatabaseHelper.COLUMN_RUSSIAN, DatabaseHelper.COLUMN_TRANSCRIPTION,
                DatabaseHelper.COLUMN_STATE, DatabaseHelper.COLUMN_COUNT, DatabaseHelper.COLUMN_PARTSOFSPEECH, DatabaseHelper.COLUMN_THEME);

        String sql = "INSERT INTO " + DatabaseHelper.TABLE_WORDS + " VALUES(?,?,?,?,?,?,?);";
        SQLiteStatement statement = database.compileStatement(insertQuery);
        database .beginTransaction();
        try {
            for (int i = 0; i < english.size(); i++){

                statement.clearBindings();
                statement.bindString( 1, english.get(i));
                statement.bindString( 2, russian.get(i));
                if(transcription.get(i) != null)
                    statement.bindString( 3, transcription.get(i));
                statement.bindString( 4, state.get(i).toString());
                statement.bindLong(5, LBApplication.PREF_COUNT_REPEAT); //2
                if(parts.get(i) != PartOfSpeech.NONE)
                    statement.bindString( 6, parts.get(i).toString());
                if(theme.get(i) != null)
                    statement.bindString( 7, theme.get(i));
                statement.execute();
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }
    }

    public Cursor getCursor(CharSequence constraint, String locale){
        Cursor mCursor;
        if (constraint == null || constraint.length() == 0) {
            String query = String.format("SELECT * FROM %s", DatabaseHelper.TABLE_WORDS);
            mCursor = database.rawQuery(query, null);
        } else { //TODO подумать над запросом, сейчас находит любое совпадение последовательности, не обязательно в начале
            //String query = String.format("SELECT * FROM %s WHERE %s LIKE ?", DatabaseHelper.TABLE_WORDS, DatabaseHelper.COLUMN_ENGLISH);
            //mCursor = database.rawQuery(query, new String[]{"%" + constraint.toString() + "%"});
            //РАСШИРИМ для поиска русских слов

            if(locale.equals("ru")){
                String query = String.format("SELECT * FROM %s WHERE %s LIKE ?", DatabaseHelper.TABLE_WORDS, DatabaseHelper.COLUMN_RUSSIAN);
                mCursor = database.rawQuery(query, new String[]{"%" + constraint.toString() + "%"});
            }
            else { //TODO плохо - надо, чтоб если язык неизвестен кидать исключение или что в этом духе, а так у нас , если язык не русский, то хоть там китайский все равно поиск в англ. столбце
                String query = String.format("SELECT * FROM %s WHERE %s LIKE ?", DatabaseHelper.TABLE_WORDS, DatabaseHelper.COLUMN_ENGLISH);
                mCursor = database.rawQuery(query, new String[]{"%" + constraint.toString() + "%"});
            }



        }

        return mCursor;
    }

    public void deleteDuplicate(){
        String insertQuery = String.format("DELETE FROM %s WHERE %s NOT IN (SELECT MIN(%s) FROM %s GROUP BY %s || %s)",
                DatabaseHelper.TABLE_WORDS, DatabaseHelper.COLUMN_ID, DatabaseHelper.COLUMN_ID, DatabaseHelper.TABLE_WORDS,
                DatabaseHelper.COLUMN_ENGLISH, DatabaseHelper.COLUMN_RUSSIAN);
        database.execSQL(insertQuery);
    }

    //удаляем все записи из БД
    public void clearBD(){
        String insertQuery = String.format("DELETE FROM %s", DatabaseHelper.TABLE_WORDS);
        database.execSQL(insertQuery);
    }

    //возвращаем БД к первоначальному состоянию прогресса (меняется только состояние и число повторов для запоминания), добавленные слова не удаляются
    public void resetBD(){
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.COLUMN_STATE, StateCard.READY.toString()); //статус в готовность
        cv.put(DatabaseHelper.COLUMN_COUNT, LBApplication.PREF_COUNT_REPEAT);
        database.update(DatabaseHelper.TABLE_WORDS, cv, null, null);
    }



}
