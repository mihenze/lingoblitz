package com.svarogsoft.lingoblitz.data;

import android.content.Context;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DataAppHelper {
    private static final String DATA_APP_NAME = "dataEB.json"; //название файла
    private static String DATA_APP_PATH;
    private final Context myContext;

    public DataAppHelper(Context context){
        myContext = context;
        DATA_APP_PATH = "/data/data/" + context.getPackageName() + "/myapp/" + DATA_APP_NAME;
        copyDataApp();
    }

    private void copyDataApp(){
        if(!checkDataApp()){
            try {
                copyDataAppFile();
            } catch (IOException e) {
                throw new Error("ErrorCopyingDataApp"); //ошибка копирования файла данных приложения на устройство
            }
        }
    }

    private void copyDataAppFile() throws IOException {
        //получаем локальный файл данных приложения как поток
        InputStream inputStream = myContext.getAssets().open(DATA_APP_NAME);
        //открываем пустой файл
        //FileOutputStream s = FileUtils.openOutputStream(new File("/home/nikhil/somedir/file.txt"))
        OutputStream outputStream = new FileOutputStream(DATA_APP_PATH);
        //побайтово копируем
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) > 0) {
            outputStream.write(buffer, 0, length);
        }

        outputStream.flush();
        outputStream.close();
        inputStream.close();
    }

    private boolean checkDataApp(){
        File daFile = new File(DATA_APP_PATH);
        boolean isFile = daFile.exists();
        if(!isFile)
            try {
                daFile.getParentFile().mkdirs();
                daFile.createNewFile();
            } catch (IOException e){
                e.printStackTrace();
            }

        return isFile;
    }

    public List<String> getCaptionsFromJSON(){
        InputStreamReader streamReader = null;
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(DATA_APP_PATH);
            streamReader = new InputStreamReader(fileInputStream);
            Gson gson = new Gson();
            DataApp dataApp = gson.fromJson(streamReader, DataApp.class);
            return dataApp.getCaptions();
        } catch (IOException e){
            e.printStackTrace();
        }
        finally {

                if (streamReader != null) {
                    try {
                        streamReader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        }
        return null;
    }

    public boolean setCaptionsToJSON(List<String> captions){

        Gson gson = new Gson();
        DataApp dataApp = new DataApp();
        dataApp.setCaptions(captions);
        String jsonString = gson.toJson(dataApp);

        FileOutputStream fileOutputStream = null;
        try{
            fileOutputStream = new FileOutputStream(DATA_APP_PATH);
            fileOutputStream.write(jsonString.getBytes());
            return true;
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public void verifyAddTheme(String themes){
        //если что-то ввели
        if (themes != null) {
            //1 - парсим строку
            String[] arrayTheme = themes.split(", ");
            //TODO почему я использую здесь лист, разве в теории у меня не может быть двух одинаковых тем у одного слова - проверить!!!
            List<String> listTheme = Arrays.asList(arrayTheme);
            //2 - получаем из файла JSON сохр. данные
            List<String> listThemeJSON = getCaptionsFromJSON();
            //3 - проверяем есть ли новые темы
            //TODO для этой строки пришлось повысить уровень API на 24 (был 23) и в gradle прописать 1.8 если больше нигде не надо вернуться к API23
            //List<String> result = Stream.concat(listTheme.stream(), listThemeJSON.stream()).distinct().filter(s -> !listThemeJSON.contains(s)).collect(Collectors.toList());
            List<String> result = Stream.concat(listThemeJSON.stream(), listTheme.stream()).distinct().collect(Collectors.toList());
            if (result.size() > listThemeJSON.size()) {
                //обновляем данные в файле
                setCaptionsToJSON(result);
            }
        }
    }

    public void verifyAddTheme(List<String> themes){
        Set<String> listTheme = new HashSet<>(); //TODO а здесь SET - все правильно, отсеиваем повторяющие темы
        for (int i = 0; i < themes.size(); i++){
            //если что-то ввели
            if (themes.get(i) != null) {
                //1 - парсим строку
                String[] arrayTheme = themes.get(i).split(", ");
                Collections.addAll(listTheme, arrayTheme);
            }
        }
        //2 - получаем из файла JSON сохр. данные
        List<String> listThemeJSON = getCaptionsFromJSON();
        //3 - проверяем есть ли новые темы
        //TODO для этой строки пришлось повысить уровень API на 24 (был 23) и в gradle прописать 1.8 если больше нигде не надо вернуться к API23
        List<String> result = Stream.concat(listThemeJSON.stream(), listTheme.stream()).distinct().collect(Collectors.toList());
        if (result.size() > listThemeJSON.size()) {
            //обновляем данные в файле
            setCaptionsToJSON(result);
        }
    }

    public void verifyRemovedTheme(DatabaseAdapter databaseAdapter){
        //а вдруг что-то удалили, нужно проверить
        List<String> listThemeJSON = getCaptionsFromJSON();
        List<String> result = listThemeJSON.stream().filter(s -> databaseAdapter.isThemeAlive(s)).collect(Collectors.toList());
        if (result.size() < listThemeJSON.size()) {
            //обновляем данные в файле
            setCaptionsToJSON(result);
        }
    }
}
