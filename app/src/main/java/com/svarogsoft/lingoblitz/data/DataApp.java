package com.svarogsoft.lingoblitz.data;

import java.util.ArrayList;
import java.util.List;

public class DataApp {
    private List<String> captions = new ArrayList<>();

    public List<String> getCaptions() {
        return captions;
    }

    public void setCaptions(List<String> captions){
        this.captions = captions;
    }


}
