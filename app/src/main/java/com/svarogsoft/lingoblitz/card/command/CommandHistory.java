package com.svarogsoft.lingoblitz.card.command;

import java.util.ArrayDeque;
import java.util.Deque;

/**История выполненных команд*/

public class CommandHistory {
    private Deque<CardCommand> history = new ArrayDeque<>();
    private final int LIMIT = 5;

    public void push(CardCommand cardCommand) {
        //если размер очереди превышает предел убираем самый ранний элемент
        if(history.size() >= LIMIT){
            history.pollLast();
        }
        history.push(cardCommand);
    }

    public CardCommand pop() {
        return history.pop();
    }

    public boolean isEmpty() { return history.isEmpty(); }
}
