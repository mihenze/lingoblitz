package com.svarogsoft.lingoblitz.card;

/**Состояния карты*/

public enum StateCard {
    READY("READY"),
    ACTIVELY("ACTIVELY"),
    PASSIVELY("PASSIVELY"),
    STUDIED("STUDIED");

    private String state;

    StateCard(String state){
        this.state = state;
    }

    @Override
    public String toString() {
        return state;
    }
}
