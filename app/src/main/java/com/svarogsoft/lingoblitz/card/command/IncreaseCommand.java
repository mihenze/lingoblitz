package com.svarogsoft.lingoblitz.card.command;

import com.svarogsoft.lingoblitz.card.DeckModel;
import com.svarogsoft.lingoblitz.view.LBApplication;
import com.svarogsoft.lingoblitz.view.SettingsActivity;

/**Увеличить счетчик карты*/

public class IncreaseCommand extends CardCommand {

    public IncreaseCommand(DeckModel deckModel) {
        super(deckModel);
    }

    @Override
    public boolean execute() {
        //выполняем действия
        //первое - инкремент счетчика текущего слова
        deckModel.getCurrentCard().setCount(deckModel.getCurrentCard().getCount()+ LBApplication.PREF_COUNT_INCREASE);
        //второе - уведомляем БД
        deckModel.getDatabaseAdapter().updateCard(deckModel.getCurrentCard());
        //возвращаем изменяет ли команда объекты
        return false;
    }
}
