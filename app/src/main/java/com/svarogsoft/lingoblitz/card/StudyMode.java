package com.svarogsoft.lingoblitz.card;

/**Режимы изучения - при выборе из меню*/

public enum StudyMode {
    ALL,
    PARTS,
    THEME;
    //константа для упаковки параметра
    public static final String EXTRA_MODE = "STUDY_MODE";
    //TODO там где передаю в классы, если придет null надо ли как-то обрабатывать
}
