package com.svarogsoft.lingoblitz.card.command;

import com.svarogsoft.lingoblitz.card.DeckModel;
import com.svarogsoft.lingoblitz.card.StateCard;

/**Следующая карта*/

public class NextCommand extends CardCommand {

    public NextCommand(DeckModel deckModel) {
        super(deckModel);
    }

    @Override
    public boolean execute() {
        //сохраняем текущее состояние
        backup();
        //выполняем действия
        //декрементируем счетчик текущего слова
        deckModel.getCurrentCard().setCount(deckModel.getCurrentCard().getCount() - 1);
        if (deckModel.getCurrentCard().getCount() == 0) {//счетчик слова опустился до нуля
            //изменяем
            deckModel.getCurrentCard().setState(StateCard.STUDIED); //статус на изучено
            deckModel.getCards().set(deckModel.getIndexCard(), deckModel.getCurrentCard());

            //здесь надо уведомить БД об изменении
            deckModel.getDatabaseAdapter().updateCard(deckModel.getCurrentCard());

            //удаляем из списка
            deckModel.getCards().remove(deckModel.getIndexCard());

            //запрашиваем новое слово для колоды
            deckModel.takeCards(1);

        }
        //получаем следующую карту из колоды
        getCard();
        //возвращаем изменяет ли команда объекты
        return true;
    }

}
