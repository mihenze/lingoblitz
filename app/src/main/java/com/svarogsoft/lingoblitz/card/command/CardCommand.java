package com.svarogsoft.lingoblitz.card.command;

import com.svarogsoft.lingoblitz.card.Card;
import com.svarogsoft.lingoblitz.card.DeckModel;

import java.util.ArrayList;
import java.util.List;

//нужно для Undo/redo
//Класс паттерна Снимок (Memento) для DataBaseModel не подошел, так как сложная логика если команда никак не меняет карту (см. ниже - другие дейсвтия)
//потому что именно модель должна делать снимок, а для этого ей нужно определить подвид команды - есть смысл попробовать иначе только одной командой
//см. refactoring.guru
//думаю должно быть чертыре команды: Взять следующу карту, Вернуть предыдущую, Инкрементировать текущую карту, Пассануть текущую
//другие действия не должны на это влиять (перевернуть карту, редактирование карты или добавление в избранное)

/**Абстрактный класс команды*/

public abstract class CardCommand {
    protected DeckModel deckModel; //ссылка на модель
    //бекапы переменных
    private List<Card> cardsBackup = new ArrayList<>(); //список наших карт
    private int indexCardBackup; //текущий индекс

    CardCommand(DeckModel deckModel){
        this.deckModel = deckModel;
    }

    void backup(){
        for(Card card : deckModel.getCards()) {
            cardsBackup.add(new Card(card));
        }
        indexCardBackup = deckModel.getIndexCard();
    }

    public void undo(){
        deckModel.restore(cardsBackup, indexCardBackup);
    }

    void getCard(){
        //TODO статическое получение карты
        //инкрементируем значение и проверяем, если до предела, то начинаем заново
        deckModel.setIndexCard(deckModel.getIndexCard()+1);
        if(deckModel.getIndexCard() > deckModel.getCountCards()-1) deckModel.setIndexCard(0);

        //TODO рандомное получение карты - частые повторы на малых числах
        /*if(deckModel.getCountCards() > 0){
            deckModel.setIsCards(true);
            int index = ThreadLocalRandom.current().nextInt(0, deckModel.getCountCards());
            deckModel.setIndexCard(index);
            deckModel.setCurrentCard(deckModel.getCards().get(deckModel.getIndexCard()));
        }
        else deckModel.setIsCards(false);*/

    }

    public abstract boolean execute();
}
