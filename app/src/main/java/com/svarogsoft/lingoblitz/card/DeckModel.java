package com.svarogsoft.lingoblitz.card;

import android.content.Context;

import com.svarogsoft.lingoblitz.card.command.CardCommand;
import com.svarogsoft.lingoblitz.card.command.CommandHistory;
import com.svarogsoft.lingoblitz.card.command.EditCommand;
import com.svarogsoft.lingoblitz.card.command.IncreaseCommand;
import com.svarogsoft.lingoblitz.card.command.NextCommand;
import com.svarogsoft.lingoblitz.card.command.PassCommand;
import com.svarogsoft.lingoblitz.data.DatabaseAdapter;
import com.svarogsoft.lingoblitz.view.LBApplication;
import com.svarogsoft.lingoblitz.view.SettingsActivity;

import java.util.List;

/** Колода карт
 * ПРОСЛОЙКА между адаптером и презентером
 *  хроанит колоду, индекс текущей открытой карты и прочие значения
 *  выполняет команды - след. карта, сбросить, увеличить, редакт, отменить
 *  а также востанавливает себя из бэкапа команды */
public class DeckModel {

    private List<Card> cards; //список наших карт
    private DatabaseAdapter databaseAdapter; //адаптер для работы с БД
    private StudyMode studyMode; //режим изучения
    private PartOfSpeech partOfSpeechMode;
    private String themeMode;
    private CommandHistory history = new CommandHistory(); //история команд
    private int indexCard = -1; //текущий индекс

    public DeckModel(Context context, StudyMode studyMode){
        databaseAdapter = new DatabaseAdapter(context);
        this.studyMode = studyMode;
        initCards(); //инициализируем наш контейнер
        if (cards.size() > 0) indexCard = 0;


    }

    public DeckModel(Context context, StudyMode studyMode, PartOfSpeech partOfSpeechMode){
        databaseAdapter = new DatabaseAdapter(context);
        this.studyMode = studyMode;
        this.partOfSpeechMode = partOfSpeechMode;
        initCards(); //инициализируем наш контейнер
        if (cards.size() > 0) indexCard = 0;
    }

    public DeckModel(Context context, StudyMode studyMode, String themeMode){
        databaseAdapter = new DatabaseAdapter(context);
        this.studyMode = studyMode;
        this.themeMode = themeMode;
        initCards(); //инициализируем наш контейнер
        if (cards.size() > 0) indexCard = 0;
    }

    private void initCards(){
        switch (studyMode){
            case ALL: //TODO isFirstRequest - изменить логику, так как она все равно не иделаьна
                cards = databaseAdapter.getCardsModeAll(LBApplication.PREF_COUNT_CARD, true);
                break;
            case PARTS:
                cards = databaseAdapter.getCardsModeParts(LBApplication.PREF_COUNT_CARD, partOfSpeechMode, true);
                break;
            case THEME:
                cards = databaseAdapter.getCardsModeTheme(LBApplication.PREF_COUNT_CARD, themeMode, true);
                break;
        }
    }
    //достатть карты из БД и поместить в колоду
    public void takeCards(int countCards){
        //карт может и не быть
        switch (studyMode){
            case ALL:
                cards.addAll(databaseAdapter.getCardsModeAll(countCards, false));
                break;
            case PARTS:
                cards.addAll(databaseAdapter.getCardsModeParts(countCards, partOfSpeechMode, false));
                break;
            case THEME:
                cards.addAll(databaseAdapter.getCardsModeTheme(countCards, themeMode, false));
                break;
        }
    }

    public void closeCards(){
        //уведомляем БД о закрытии, чтоб обновить текущие изменения колоды
        databaseAdapter.updateCountCards(cards);
    }

    public boolean actionCard(ActionCardMode cardMode){
        switch (cardMode) {
            case NEXT:
                executeCommand(new NextCommand(this));
                break;
            case PREVIOUS: undoCommand(); break;
            case TOUCH: break;
            case PASS:
                executeCommand(new PassCommand(this));
                break;
            case INCREASE:
                executeCommand(new IncreaseCommand(this));
                break;
        }

        return isCards();
    }

    private void executeCommand(CardCommand cardCommand){
        if (isCards()) { //карты еще есть в колоде
            if (cardCommand.execute()) {
                history.push(cardCommand);
            }
        }
    }

    private void undoCommand(){
        if (history.isEmpty()) return;
        CardCommand cardCommand = history.pop();
        if(cardCommand!=null){
            cardCommand.undo();
        }
    }

    public void restore(List<Card> cards, int indexCard){

        //если в текущей колоде нет карты из бекапа, значит мы изучили это слово и должны
        //обновить в БД инофрмацию о ней обратно(выставить, что слово не изучено и счетчик)
        //if(!this.cards.contains(cards.get(indexCard))){
         //   databaseAdapter.updateStateCard(cards.get(indexCard));
        //}

        //обновляем инфу в базе по текущей карте
        databaseAdapter.updateCard(cards.get(indexCard));

        //если в колоде были новые слова в отличии от еолоды бекапа
        //надо воостановить состояние этих слов в готовность
        //счетчик менять не надо, так как UNDO -лишь один шаг и мы не успели его изменить
        this.cards.removeAll(cards);
        if(this.cards.size() > 0){
            databaseAdapter.updateStateCards(this.cards, StateCard.READY, StateCard.ACTIVELY);
        }
        this.cards.clear();
        //присваиваем переданное
        this.cards = cards;
        this.indexCard = indexCard;
    }

    public List<Card> getCards() {
        return cards;
    }

    public Card getCurrentCard() {
        return cards.get(indexCard);
    }

    public DatabaseAdapter getDatabaseAdapter() {
        return databaseAdapter;
    }

    public StudyMode getStudyMode() {
        return studyMode;
    }

    public boolean isCards() {
        return cards.size()>0;
    }

    public int getIndexCard() {
        return indexCard;
    }

    public int getCountCards() {
        return cards.size();
    }

    public void setIndexCard(int indexCard) {
        this.indexCard = indexCard;
    }

    public String getNativeWord() {
        return cards.get(indexCard).getNativeWord();
    }

    public String getStudiedWord() {
        return cards.get(indexCard).getStudiedWord();
    }

    public String getTranscription() {
        return cards.get(indexCard).getTranscription();
    }

    public int getCount() {return cards.get(indexCard).getCount(); }

    public PartOfSpeech getPartOfSpeech(){
        PartOfSpeech temp = cards.get(indexCard).getPartOfSpeech();
        return (temp==null)? PartOfSpeech.NONE : temp;
    }

    public void setWordEngRusCurrentCard(String englishWord, String russianWord){
        //выполняем команду отредактировать
        executeCommand(new EditCommand(this, englishWord, russianWord));
    }


}
