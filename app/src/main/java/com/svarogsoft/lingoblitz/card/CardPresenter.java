package com.svarogsoft.lingoblitz.card;

import android.content.Context;

/**Реализация MVP - Presenter между моделью колоды карт и представлением CardActivity*/

public class CardPresenter {
    private StudyMode studyMode; //режим изучения
    ICardView iCardView;
    DeckModel deckModel;
    boolean isReverse = false; //

    public CardPresenter(ICardView iCardView, StudyMode studyMode, Context context){
        this.iCardView = iCardView; //TODO можно перенести в метод attach? надоли?
        this.studyMode = studyMode;
        deckModel = new DeckModel(context, studyMode);
        showCard(ActionCardMode.TOUCH); //искуственно вызыванием касание, чтоб отобразить карту
    }

    public CardPresenter(ICardView iCardView, StudyMode studyMode, PartOfSpeech partOfSpeechMode, Context context){
        this.iCardView = iCardView; //TODO можно перенести в метод attach? надоли?
        this.studyMode = studyMode;
        deckModel = new DeckModel(context, studyMode, partOfSpeechMode);
        showCard(ActionCardMode.TOUCH); //искуственно вызыванием касание, чтоб отобразить карту
    }

    public CardPresenter(ICardView iCardView, StudyMode studyMode, String themeMode, Context context){
        this.iCardView = iCardView; //TODO можно перенести в метод attach? надоли?
        this.studyMode = studyMode;
        deckModel = new DeckModel(context, studyMode, themeMode);
        showCard(ActionCardMode.TOUCH); //искуственно вызыванием касание, чтоб отобразить карту
    }

    public void touchCard(){
        isReverse = !isReverse;
        showCard(ActionCardMode.TOUCH);
    }

    public void showCard(ActionCardMode cardMode){

        if(deckModel.actionCard(cardMode)) {
            if (isReverse) {
                iCardView.showText(deckModel.getNativeWord(), "", deckModel.getPartOfSpeech());
            } else {
                iCardView.showText(deckModel.getStudiedWord(), deckModel.getTranscription(), deckModel.getPartOfSpeech());
            }
            //TODO Временное решение об уведомлении части действий
            iCardView.showToast(cardMode);

            //Фон карты
            if(deckModel.getCount() == 1){
                iCardView.showBackground(false);
            }
            else {
                iCardView.showBackground(true);
            }

        }
        else { //TODO надо уведомить view чтоб отобразила окно о том, что слова изучены, а пока просто показываем на карточке это сообщение
            iCardView.showText("!All words are learned!", "!Все слова изучены!", PartOfSpeech.NONE);
        }


    }

    public void close(){
        deckModel.closeCards(); //закрываем колоду карт, для сохранения изменений
    }

    public String getStudiedWord(){
        if (deckModel == null || !deckModel.isCards()){
            return "";
        }
        else
            return deckModel.getStudiedWord();
    }

    public String getNativeWord(){
        if (deckModel == null || !deckModel.isCards()){
            return "";
        }
        else
            return deckModel.getNativeWord();
    }

    public boolean isCards(){
        return deckModel.isCards();
    }

    public void isCardEdit(){
        //нужно взять данные из View и передать в модель
        deckModel.setWordEngRusCurrentCard(iCardView.getEditStudiedWord(), iCardView.getEditNativeWord());
        //обновим данные и для показа в вьюхе
        showCard(ActionCardMode.TOUCH);
    }

}
