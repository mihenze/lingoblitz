package com.svarogsoft.lingoblitz.card.command;

import com.svarogsoft.lingoblitz.card.DeckModel;
import com.svarogsoft.lingoblitz.card.StateCard;

/**Сбросить карту*/

public class PassCommand extends CardCommand{

    public PassCommand(DeckModel deckModel) {
        super(deckModel);
    }

    @Override
    public boolean execute() {
        //сохраняем текущее состояние
        backup();
        //выполняем действия
        //первое - выставляем статус карты на пассивное
        deckModel.getCurrentCard().setState(StateCard.PASSIVELY);
        //второе - уведомляем БД
        deckModel.getDatabaseAdapter().updateCard(deckModel.getCurrentCard());
        //третье - удаляем из списка
        deckModel.getCards().remove(deckModel.getIndexCard());
        //четвертое - запрашиваем новое слово для колоды
        deckModel.takeCards(1);
        //получаем следующую карту из колоды
        getCard();
        //возвращаем изменяет ли команда объекты
        return true;
    }
}
