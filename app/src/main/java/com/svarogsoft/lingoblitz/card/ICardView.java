package com.svarogsoft.lingoblitz.card;

//MVP - общий интерфейс для всех View

public interface ICardView {
    void showText(String word, String transcription, PartOfSpeech partOfSpeechWord);
    void showToast(ActionCardMode mode);
    void showBackground(boolean isNormal);
    String getEditStudiedWord();
    String getEditNativeWord();
}
