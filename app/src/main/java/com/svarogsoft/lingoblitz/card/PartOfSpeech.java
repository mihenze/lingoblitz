package com.svarogsoft.lingoblitz.card;

/**Части речи (перечисление)
 * НЕ УСТРАИВАЕТ NULL - разобраться*/

public enum PartOfSpeech {
    NONE(null),
    NOUN("Noun"),
    ADJECTIVE("Adjective"),
    VERB("Verb");

    private String type;

    PartOfSpeech(String type) { this.type = type; }

    @Override
    public String toString() {
        return type;
    }
    //константа для упаковки параметра
    public static final String PARTS_OF_SPEECH_MODE = "PART_OF_SPEECH_MODE";
}
