package com.svarogsoft.lingoblitz.card;

import java.util.Objects;

/**Класс карты*/

public class Card {
    private long id; //уникальный идентификатор в БД
    private String studiedWord; //слово на изучаемом языке
    private String nativeWord; //слово на родном языке
    private String transcription = ""; //транскрипция на английском
    private StateCard state; //состояние слова для изучения: пассивно(не требует изучения), активно(стд. изучение), интенсивно(частое изучение)
    private int count; //счетчик изучения слова
    private PartOfSpeech partOfSpeech = PartOfSpeech.NONE; //принадлежность к части речи
    private String theme = ""; //принадлежность к теме/темам

    //TODO константы для упаковки и передачи между активностями
    public static final String ID_KEY = "ID";
    public static final String STUDIED_KEY= "STUDIED";
    public static final String NATIVE_KEY = "NATIVE";
    public static final String TRANSCRIPTION_KEY = "TRANSCRIPTION";
    public static final String STATE_KEY = "STATE";
    public static final String PARTS_KEY = "PARTS";
    public static final String THEME_KEY = "THEME";

    //конструктор обязательных частей
    public Card(long id, String studiedWord, String nativeWord, StateCard state, int count){
        this.id = id;
        this.studiedWord = studiedWord;
        this.nativeWord = nativeWord;
        this.state = state;
        this.count = count;
    }

    //конструктор всех частей
    public Card(long id, String studiedWord, String nativeWord, StateCard state, int count, String transcription, PartOfSpeech partOfSpeech, String theme){
        this(id, studiedWord, nativeWord, state, count);
        this.transcription = transcription;
        this.partOfSpeech = partOfSpeech;
        this.theme = theme;
    }

    public Card(Card another){
        this(another.id, another.studiedWord, another.nativeWord, another.state, another.count, another.transcription, another.partOfSpeech, another.theme);
    }

    public long getId() {
        return id;
    }

    public String getStudiedWord() {
        return studiedWord;
    }

    public void setStudiedWord(String studiedWord) {
        this.studiedWord = studiedWord;
    }

    public String getNativeWord() {
        return nativeWord;
    }

    public void setNativeWord(String nativeWord) {
        this.nativeWord = nativeWord;
    }

    public String getTranscription() {
        return transcription;
    }

    public void setTranscription(String transcription) {
        this.transcription = transcription;
    }

    public StateCard getState() {
        return state;
    }

    public void setState(StateCard state) {
        this.state = state;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public PartOfSpeech getPartOfSpeech() {
        return partOfSpeech;
    }

    public void setPartOfSpeech(PartOfSpeech partOfSpeech) {
        this.partOfSpeech = partOfSpeech;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return id == card.id &&
                Objects.equals(studiedWord, card.studiedWord) &&
                Objects.equals(nativeWord, card.nativeWord);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, studiedWord, nativeWord);
    }
}
