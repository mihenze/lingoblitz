package com.svarogsoft.lingoblitz.card;


import android.view.GestureDetector;
import android.view.MotionEvent;

/**Логика жестов - управление движением пальцев, собственный слушатель для карточки*/

                               //implements GestureDetector.OnGestureListener
public class CardGestureListener extends GestureDetector.SimpleOnGestureListener {

    //пороговая скорость
    private static final long VELOCITY_THRESHOLD = 100;
    //Presenter получающий уведомления от действий пользователя
    private final CardPresenter presenter;

    public CardGestureListener(CardPresenter presenter){ //TODO если пришел nool надо ли бросать исключение?
        this.presenter = presenter;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {
        super.onShowPress(e);
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        presenter.touchCard(); //сообщаем, что произошло касание
        return super.onSingleTapUp(e);
    }



    @Override
    public void onLongPress(MotionEvent e) {
        super.onLongPress(e);
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

        //если движение не достаточно быстрое, то не считаем за свайп(как перетаскивание)
        if(Math.abs(velocityX) < VELOCITY_THRESHOLD
                && Math.abs(velocityY) < VELOCITY_THRESHOLD){
            return false;
        }

        //В зависимости от большей скорости определяем направление
        //горизонатльное направление
        if(Math.abs(velocityX) > Math.abs(velocityY)){
            if(velocityX >= 0){
                //вправо
                //presenter.showCard(ActionCardMode.NEXT);
                presenter.showCard(ActionCardMode.PREVIOUS);
            }else{
                //влево
                //presenter.showCard(ActionCardMode.PREVIOUS);
                presenter.showCard(ActionCardMode.NEXT);
            }
        }
        //вертикальное направление
        else{
            if(velocityY >= 0){
                //вниз
                presenter.showCard(ActionCardMode.PASS);
            }else{
                //вверх
                presenter.showCard(ActionCardMode.INCREASE);
            }
        }

        return true;
    }
}
