package com.svarogsoft.lingoblitz.card.command;

import com.svarogsoft.lingoblitz.card.DeckModel;

/**Изменить поля слов карты*/

public class EditCommand extends CardCommand {
    private String english;
    private String russian;
    public EditCommand(DeckModel deckModel, String english, String russian) {
        super(deckModel);
        this.english = english;
        this.russian = russian;
    }

    @Override
    public boolean execute() {
        //сохраняем текущее состояние
        backup();
        //выполняем действия
        //изменяем данные
        deckModel.getCurrentCard().setStudiedWord(english);
        deckModel.getCurrentCard().setNativeWord(russian);
        //второе - уведомляем БД
        deckModel.getDatabaseAdapter().updateCard(deckModel.getCurrentCard());
        //возвращаем изменяет ли команда объекты
        return true;
    }
}
