package com.svarogsoft.lingoblitz.card;

/**Класс для действий над картой при изучении*/

public enum ActionCardMode {
    NEXT,
    PREVIOUS,
    TOUCH,
    PASS,
    INCREASE,
}
