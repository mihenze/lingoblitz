package com.svarogsoft.lingoblitz.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.svarogsoft.lingoblitz.R;
import com.svarogsoft.lingoblitz.card.PartOfSpeech;
import com.svarogsoft.lingoblitz.card.StudyMode;

public class SelectionActivity extends AppCompatActivity {

    private StudyMode studyMode;
    private PartOfSpeech typeWordMode;
    private String themeMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);

        //убираем шторку
        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //кнопка назад
        Button buttonBack = (Button)findViewById(R.id.button_back);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //определяем режим
        initStudyMode();



        //кнопка Карточки
        Button buttonCards = (Button)findViewById(R.id.button_cards);
        buttonCards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    Intent intent = new Intent(SelectionActivity.this, CardActivity.class);
                    intent.putExtra(StudyMode.EXTRA_MODE, studyMode);

                    switch (studyMode){
                        case ALL: break;
                        case PARTS:
                            intent.putExtra(PartOfSpeech.PARTS_OF_SPEECH_MODE, typeWordMode);
                            break;
                        case THEME:
                            intent.putExtra(ThemeActivity.THEME_MODE, themeMode);
                            break;
                    }

                    startActivity(intent); finish();
                } catch (Exception e){

                }
            }
        });
    }

    private void initStudyMode(){
        //получаем тип изучения
        studyMode = (StudyMode)getIntent().getSerializableExtra(StudyMode.EXTRA_MODE);
        switch (studyMode){
            case ALL: break;
            case PARTS:
                typeWordMode = (PartOfSpeech)getIntent().getSerializableExtra(PartOfSpeech.PARTS_OF_SPEECH_MODE);
                break;
            case THEME:
                themeMode = (String)getIntent().getSerializableExtra(ThemeActivity.THEME_MODE);
                break;
        }
    }

    //Системная кнопка назад
    @Override
    public void onBackPressed() {
        try {
            if(studyMode == StudyMode.ALL) {
                Intent intent = new Intent(SelectionActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
            else if (studyMode == StudyMode.PARTS) {
                Intent intent = new Intent(SelectionActivity.this, PartsActivity.class);
                startActivity(intent);
                finish();
            }
            else if (studyMode == StudyMode.THEME) {
                Intent intent = new Intent(SelectionActivity.this, ThemeActivity.class);
                startActivity(intent);
                finish();
            }
            else {
                Intent intent = new Intent(SelectionActivity.this, SettingsActivity.class);
                startActivity(intent);
                finish();
            }
        } catch (Exception e){
            //TODO если не открылись и бог с ним
        }
    }
}
