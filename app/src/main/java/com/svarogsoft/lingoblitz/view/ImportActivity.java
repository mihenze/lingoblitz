package com.svarogsoft.lingoblitz.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.svarogsoft.lingoblitz.R;
import com.svarogsoft.lingoblitz.dictionary.IImportView;
import com.svarogsoft.lingoblitz.dictionary.ImportPresenter;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ImportActivity extends AppCompatActivity implements IImportView {
    //кнопка для открытия списка шаблона
    FloatingActionButton fab;
    //набор шаблонов
    CardView fab1;
    CardView fab2;
    CardView fab3;
    CardView fab4;
    CardView fab5;
    CardView fab6;
    //отображается ли набор шаблонов
    private boolean FAB_Status = false;
    //Animations
    Animation show_fab_1;
    Animation hide_fab_1;
    Animation show_fab_2;
    Animation hide_fab_2;
    Animation show_fab_3;
    Animation hide_fab_3;
    Animation show_fab_4;
    Animation hide_fab_4;
    Animation show_fab_5;
    Animation hide_fab_5;
    Animation show_fab_6;
    Animation hide_fab_6;
    //слой для шаблона
    FrameLayout temlateLayout;

    Map<CardView, Boolean> isFab = new LinkedHashMap<>(); //флаги выбора
    Map<CardView, Point> pointFab = new LinkedHashMap<>(); //точки положения
    List<Point> pointFabChoose = new ArrayList<>(); //точки куда встать
    //середина по горизонтали
    int x_width;

    ImportPresenter importPresenter; //посредник между отображением и данными
    private EditText inputSeparator; // разделитель
    private Toast noticeToast; //всплывающее сообщение
    private String importPath; //путь к файлу

    public static final int REQUEST_FILE_DIALOG = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_import);

        //убираем шторку
        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //кнопка назад
        Button buttonBack = (Button)findViewById(R.id.button_back);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //Создаем presenter для импорта
        importPresenter = new ImportPresenter(this, getApplicationContext());

        //кнопка импорт
        Button buttonImport = (Button) findViewById(R.id.button_import);
        buttonImport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //уведомляем презентер об этом событии
                importPresenter.isImport();
            }
        });

        //поле ввода для разделителя
        inputSeparator = (EditText) findViewById(R.id.input_separate);
        //слой для шаблона
        temlateLayout = (FrameLayout) findViewById(R.id.template_layout);

        ViewTreeObserver vto = temlateLayout.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                temlateLayout.getViewTreeObserver().removeOnPreDrawListener(this); // удаляем листенер, иначе уйдём в бесконечный цикл
                x_width = temlateLayout.getWidth()/2 - fab1.getWidth()/2 ; //второе значение не видно еще, но оно должно тоже задаваться в константах
                initWidth();
                return true;
            }
        });

        //Floating Action Buttons
        fab = (FloatingActionButton) findViewById(R.id.fabutton_template);
        fab1 = (CardView) findViewById(R.id.fab_1);
        fab2 = (CardView) findViewById(R.id.fab_2);
        fab3 = (CardView) findViewById(R.id.fab_3);
        fab4 = (CardView) findViewById(R.id.fab_4);
        fab5 = (CardView) findViewById(R.id.fab_5);
        fab6 = (CardView) findViewById(R.id.fab_6);

        //выставляем флаг в false что части слваря не выбраны
        isFab.put(fab1, false);
        isFab.put(fab2, false);
        isFab.put(fab3, false);
        isFab.put(fab4, false);
        isFab.put(fab5, false);
        isFab.put(fab6, false);

        //Animations
        show_fab_1 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_show);
        hide_fab_1 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab1_hide);
        show_fab_2 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab2_show);
        hide_fab_2 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab2_hide);
        show_fab_3 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab3_show);
        hide_fab_3 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab3_hide);
        show_fab_4 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab4_show);
        hide_fab_4 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab4_hide);
        show_fab_5 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab5_show);
        hide_fab_5 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab5_hide);
        show_fab_6 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab6_show);
        hide_fab_6 = AnimationUtils.loadAnimation(getApplication(), R.anim.fab6_hide);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (FAB_Status == false) {
                    //Display FAB menu
                    expandFAB();
                    FAB_Status = true;
                } else {
                    //Close FAB menu
                    hideFAB();
                    FAB_Status = false;
                }
            }
        });

        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseFAB(v);
            }
        });

        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseFAB(v);
            }
        });

        fab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseFAB(v);
            }
        });

        fab4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseFAB(v);
            }
        });

        fab5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseFAB(v);
            }
        });

        fab6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseFAB(v);
            }
        });
    }

    private void initWidth(){
        //инициализация значений для полей
        //TODO подумать как определять эти значения для разных экранов, уйти от магических чисел!!!
        pointFabChoose.add(new Point(x_width, 20));
        pointFabChoose.add(new Point(x_width, 120));
        pointFabChoose.add(new Point(x_width, 220));
        pointFabChoose.add(new Point(x_width, 320));
        pointFabChoose.add(new Point(x_width, 420));
        pointFabChoose.add(new Point(x_width, 520));
    }

    //TODO подумать как перемещать 2 варианта - 1) создавать новые(можно сколько угодно скипов, сложней реализация); 2)использовать имеющиеся(проще, но все элементы по 1экз)
    /**Второй вариант - просто перемещение объектов*/
    private void chooseFAB(View v){
        CardView current = (CardView) v;
        //Если не нажат, то
        if(!isFab.get(current)){
            //Запоминаем положение если надо
            Point oldPoint = new Point((int) current.getX(), (int) current.getY());

            //Добавляем в список
            pointFab.put(current, oldPoint);

            //Берем новое значение
            Point newPoint = pointFabChoose.get(pointFab.size() - 1);

            AnimatorSet animatorSet = animationMove(current, oldPoint, newPoint);
            animatorSet.start();

        } else { //востанавливаем значения
            //берем начальную координату
            Point oldPoint = new Point((int) current.getX(), (int) current.getY());
            //Берем конечную координату и удаляем из списка
            Point newPoint = pointFab.remove(current);

            AnimatorSet animatorSet = animationMove(current, oldPoint, newPoint);
            animatorSet.start();

            animationRemove(newPoint);
        }

        //меняем флаг
        isFab.replace(current, !isFab.get(current));

    }

    private AnimatorSet animationMove(CardView moveCard, Point start, Point end){
        AnimatorSet animatorSet = new AnimatorSet();

        ValueAnimator first_x = ValueAnimator.ofInt(start.x, end.x);
        first_x.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int value =  (int) animation.getAnimatedValue();
                moveCard.setX(value);
            }
        });

        first_x.setInterpolator(new LinearInterpolator());
        first_x.setDuration(300);

        ValueAnimator first_y = ValueAnimator.ofInt(start.y, end.y);
        first_y.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int value = (int) animation.getAnimatedValue();
                moveCard.setY(value);
            }
        });

        first_y.setInterpolator(new LinearInterpolator());
        first_y.setDuration(300);

        animatorSet.play(first_x).with(first_y);
        return animatorSet;
    }

    private void animationRemove(Point removePoint){
        int position = 0;
        for(Map.Entry<CardView, Point> pair : pointFab.entrySet()){
            //int x = (int) pair.getKey().getX();
            int y = (int) pair.getKey().getY();
            if(!(y == pointFabChoose.get(position).y)){
                ValueAnimator anim = ValueAnimator.ofInt(y, pointFabChoose.get(position).y);
                anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        int value = (int) animation.getAnimatedValue();
                        pair.getKey().setY(value);
                    }
                });

                anim.setInterpolator(new LinearInterpolator());
                anim.setDuration(100);

                anim.start();
            }
            position++;


        }

    }

    //Системная кнопка назад
    @Override
    public void onBackPressed() {
        try {
            Intent intent = new Intent(ImportActivity.this, MenuDictionaryActivity.class);
            startActivity(intent); finish();
        } catch (Exception e){
            //если не открылись и бог с ним
        }
    }

    private void expandFAB() {
        if(!isFab.get(fab1)) {
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) fab1.getLayoutParams();
            layoutParams.rightMargin += (int) (fab1.getWidth() * 0.9);
            layoutParams.bottomMargin += (int) (fab1.getHeight() * 0.1);
            fab1.setLayoutParams(layoutParams);
            fab1.startAnimation(show_fab_1);
            //fab1.setClickable(true);
        }
        fab1.setClickable(true);

        //Floating Action Button 2
        if(!isFab.get(fab2)) {
            FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) fab2.getLayoutParams();
            layoutParams2.rightMargin += (int) (fab2.getWidth() * 1);
            layoutParams2.bottomMargin += (int) (fab2.getHeight() * 1.2);
            fab2.setLayoutParams(layoutParams2);
            fab2.startAnimation(show_fab_2);
            //fab2.setClickable(true);
        }
        fab2.setClickable(true);

        //Floating Action Button 3
        if(!isFab.get(fab3)) {
            FrameLayout.LayoutParams layoutParams3 = (FrameLayout.LayoutParams) fab3.getLayoutParams();
            layoutParams3.rightMargin += (int) (fab3.getWidth() * 0.9);
            layoutParams3.bottomMargin += (int) (fab3.getHeight() * 2.3);
            fab3.setLayoutParams(layoutParams3);
            fab3.startAnimation(show_fab_3);
            //fab3.setClickable(true);
        }
        fab3.setClickable(true);

        //Floating Action Button 4
        if(!isFab.get(fab4)) {
            FrameLayout.LayoutParams layoutParams4 = (FrameLayout.LayoutParams) fab4.getLayoutParams();
            layoutParams4.leftMargin += (int) (fab4.getWidth() * 0.9);
            layoutParams4.bottomMargin += (int) (fab4.getHeight() * 0.1);
            fab4.setLayoutParams(layoutParams4);
            fab4.startAnimation(show_fab_4);
            //fab4.setClickable(true);
        }
        fab4.setClickable(true);

        //Floating Action Button 5
        if(!isFab.get(fab5)) {
            FrameLayout.LayoutParams layoutParams5 = (FrameLayout.LayoutParams) fab5.getLayoutParams();
            layoutParams5.leftMargin += (int) (fab5.getWidth() * 1);
            layoutParams5.bottomMargin += (int) (fab5.getHeight() * 1.2);
            fab5.setLayoutParams(layoutParams5);
            fab5.startAnimation(show_fab_5);
            //fab5.setClickable(true);
        }
        fab5.setClickable(true);

        //Floating Action Button 6
        if(!isFab.get(fab6)) {
            FrameLayout.LayoutParams layoutParams6 = (FrameLayout.LayoutParams) fab6.getLayoutParams();
            layoutParams6.leftMargin += (int) (fab6.getWidth() * 0.9);
            layoutParams6.bottomMargin += (int) (fab6.getHeight() * 2.3);
            fab6.setLayoutParams(layoutParams6);
            fab6.startAnimation(show_fab_6);
            //fab6.setClickable(true);
        }
        fab6.setClickable(true);
    }


    private void hideFAB() {
        //Floating Action Button 1
        if(!isFab.get(fab1)) {
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) fab1.getLayoutParams();
            layoutParams.rightMargin -= (int) (fab1.getWidth() * 0.9); //1.7
            layoutParams.bottomMargin -= (int) (fab1.getHeight() * 0.1); //0.25
            fab1.setLayoutParams(layoutParams);
            fab1.startAnimation(hide_fab_1);
            //fab1.setClickable(false);
        }
        fab1.setClickable(false);


        //Floating Action Button 2
        if(!isFab.get(fab2)) {
            FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) fab2.getLayoutParams();
            layoutParams2.rightMargin -= (int) (fab2.getWidth() * 1); //1.5
            layoutParams2.bottomMargin -= (int) (fab2.getHeight() * 1.2); //1.5
            fab2.setLayoutParams(layoutParams2);
            fab2.startAnimation(hide_fab_2);
            //fab2.setClickable(false);
        }
        fab2.setClickable(false);

        //Floating Action Button 3
        if(!isFab.get(fab3)) {
            FrameLayout.LayoutParams layoutParams3 = (FrameLayout.LayoutParams) fab3.getLayoutParams();
            layoutParams3.rightMargin -= (int) (fab3.getWidth() * 0.9); //0.25
            layoutParams3.bottomMargin -= (int) (fab3.getHeight() * 2.3); //1.7
            fab3.setLayoutParams(layoutParams3);
            fab3.startAnimation(hide_fab_3);
            //fab3.setClickable(false);
        }
        fab3.setClickable(false);

        //Floating Action Button 4
        if(!isFab.get(fab4)) {
            FrameLayout.LayoutParams layoutParams4 = (FrameLayout.LayoutParams) fab4.getLayoutParams();
            layoutParams4.leftMargin -= (int) (fab4.getWidth() * 0.9);
            layoutParams4.bottomMargin -= (int) (fab4.getHeight() * 0.1);
            fab4.setLayoutParams(layoutParams4);
            fab4.startAnimation(hide_fab_4);
            //fab4.setClickable(false);
        }
        fab4.setClickable(false);

        //Floating Action Button 5
        if(!isFab.get(fab5)) {
            FrameLayout.LayoutParams layoutParams5 = (FrameLayout.LayoutParams) fab5.getLayoutParams();
            layoutParams5.leftMargin -= (int) (fab5.getWidth() * 1);
            layoutParams5.bottomMargin -= (int) (fab5.getHeight() * 1.2);
            fab5.setLayoutParams(layoutParams5);
            fab5.startAnimation(hide_fab_5);
            //fab5.setClickable(false);
        }
        fab5.setClickable(false);

        //Floating Action Button 6
        if(!isFab.get(fab6)) {
            FrameLayout.LayoutParams layoutParams6 = (FrameLayout.LayoutParams) fab6.getLayoutParams();
            layoutParams6.leftMargin -= (int) (fab6.getWidth() * 0.9);
            layoutParams6.bottomMargin -= (int) (fab6.getHeight() * 2.3);
            fab6.setLayoutParams(layoutParams6);
            fab6.startAnimation(hide_fab_6);
            //fab6.setClickable(false);
        }
        fab6.setClickable(false);
    }

    @Override
    public List<String> getTemplate() {
        List<String> result = new ArrayList<>();
        for(Map.Entry<CardView, Point> pair : pointFab.entrySet()){
            CardView cardView = pair.getKey();
            TextView textView = (TextView) cardView.getChildAt(0);
            result.add(textView.getText().toString());
        }

        return result;
    }

    @Override
    public String getSeparator() {
        return inputSeparator.getText().toString();
    }

    @Override
    public void showToast(int message) {
        noticeToast = Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT);
        noticeToast.show();
    }

    @Override
    public String getPathImportFile() {
        return importPath;
    }

    @Override
    public void startFileDialog(){
        try {
            Intent intent = new Intent(ImportActivity.this, OpenFileDialogActivity.class);
            startActivityForResult(intent, REQUEST_FILE_DIALOG);
        } catch (Exception e){
            //если не открылись и бог с ним
        }
    }

    //результат выбора файла
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK && requestCode == REQUEST_FILE_DIALOG){
            importPath = data.getStringExtra(OpenFileDialogActivity.EXTRA_FILE_PATH);
            importPresenter.isFileOpen();
        }
        else if (resultCode == REQUEST_FILE_DIALOG){
            showToast(R.string.toast_notice_import_permission);
        }
        else {
            showToast(R.string.toast_notice_import_file_not);
        }
    }
}
