package com.svarogsoft.lingoblitz.view;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Locale;

public abstract class LocalActivity extends AppCompatActivity {
    public LocalActivity() {
        //LocaleUtils.updateConfiguration(this);
        //LBApplication.getInstance().initAppLanguage(this);
    }

    // We only override onCreate
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        String s;
        if(LBApplication.PREF_APP_LANGUAGE.equals("English"))
            s = "en";
        else s = "ru";
        Locale languageType = new Locale(s);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, languageType));
    }
}
