package com.svarogsoft.lingoblitz.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FilterQueryProvider;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.svarogsoft.lingoblitz.R;
import com.svarogsoft.lingoblitz.card.Card;
import com.svarogsoft.lingoblitz.card.PartOfSpeech;
import com.svarogsoft.lingoblitz.data.DataAppHelper;
import com.svarogsoft.lingoblitz.data.DatabaseAdapter;
import com.svarogsoft.lingoblitz.card.StateCard;
import com.svarogsoft.lingoblitz.dictionary.DictionaryAdapter;

public class DictionaryActivity extends AppCompatActivity {
    //view ввода текста для поиска
    private EditText wordFilter;
    //список элементов
    RecyclerView recyclerView;
    //собственный адаптер для RecyclerView
    DictionaryAdapter dictionaryAdapter;
    //адаптер для работы с БД
    DatabaseAdapter databaseAdapter;
    //типы запросов
    private static final int REQUEST_DICTIONARY_ADD_WORD = 2; //для добавления слова
    private static final int REQUEST_DICTIONARY_EDIT_WORD = 3; //для редактирования слова

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);

        //убираем шторку
        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //отключаем автоматическое появление клавы в поле EditText
        //Добавить в AndroidManifest.xml, для нужного activity: android:windowSoftInputMode="stateHidden"
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        //кнопка назад
        Button buttonBack = (Button)findViewById(R.id.button_back);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //получаем список
        recyclerView = (RecyclerView) findViewById(R.id.list_dictionary);

        //получаем фильтр
        wordFilter = (EditText)findViewById(R.id.card_filter);

        //адаптер для работы с БД
        databaseAdapter = new DatabaseAdapter(getApplicationContext());

        //обработка нажатия по кнопке добавить
        FloatingActionButton buttonAdd = (FloatingActionButton) findViewById(R.id.fabutton_add);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddActivity();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        //try {
        //TODO зачем в двух местах одно и то же?             нужен ли try?
        //InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        //InputMethodSubtype ims = imm.getCurrentInputMethodSubtype();
        String locale = /*ims.getLocale()*/""; //при создании пустая строка
        Cursor cursor = databaseAdapter.getCursor(null, locale);
        //а) СТОЛБЦЫ которые взять из таблицы
        //б) адаптер листвью
        dictionaryAdapter = new DictionaryAdapter(cursor, new DictionaryAdapter.OnWordClickListener() {
            @Override
            public void onWordClick(long wordId) {
                onEditActivity(wordId);
            }
        });
        //в)
        // если в текстовом поле есть текст, выполняем фильтрацию
        // данная проверка нужна при переходе от одной ориентации экрана к другой
        // и при выходе из окна редактирования
        if(!wordFilter.getText().toString().isEmpty())
            dictionaryAdapter.getFilter().filter(wordFilter.getText().toString());

        // установка слушателя изменения текста
        wordFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            //при изменении текста применяем фильтрацию
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //г) фильтр адаптера
                dictionaryAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        // устанавливаем провайдер фильтрации
        dictionaryAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence constraint) {
                //определяем локаль клавиатуры ввода
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                InputMethodSubtype ims = imm.getCurrentInputMethodSubtype();
                String locale = ims.getLocale();
                return databaseAdapter.getCursor(constraint, locale); //а здесь уже с языком
            }
        });

        //} catch (SQLException e){
            //ничего
        //}
        recyclerView.setAdapter(dictionaryAdapter);
    }

    //Системная кнопка назад
    @Override
    public void onBackPressed() {
        try {
            Intent intent = new Intent(DictionaryActivity.this, MenuDictionaryActivity.class);
            startActivity(intent); finish();
        } catch (Exception e){
            //если не открылись и бог с ним
        }
    }


    //запускаем окно добавления слова
    private void onAddActivity(){
        Intent intent = new Intent(DictionaryActivity.this, EditDictionaryActivity.class);
        startActivityForResult(intent, REQUEST_DICTIONARY_ADD_WORD);
    }

    //запускаем окно редактирования слова
    private void onEditActivity(long wordId){
        Card card = databaseAdapter.getCard(wordId);
        //TODO если не получили, такое может быть ? выкидывать исключение или что?
        //упаковка данных для передачи
        Intent intent = new Intent(DictionaryActivity.this, EditDictionaryActivity.class);
        intent.putExtra(Card.ID_KEY, card.getId());
        intent.putExtra(Card.STUDIED_KEY, card.getStudiedWord());
        intent.putExtra(Card.NATIVE_KEY, card.getNativeWord());
        intent.putExtra(Card.TRANSCRIPTION_KEY, card.getTranscription());
        intent.putExtra(Card.STATE_KEY, card.getState());
        PartOfSpeech card_parts = card.getPartOfSpeech();
        if(card_parts == null)
            intent.putExtra(Card.PARTS_KEY, PartOfSpeech.NONE);
        else
            intent.putExtra(Card.PARTS_KEY, card_parts);
        intent.putExtra(Card.THEME_KEY, card.getTheme());

        startActivityForResult(intent, REQUEST_DICTIONARY_EDIT_WORD);
    }

    //результат редактирования данные после ввода данных
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){ //если результат редактирования положителен
            //определяем какое действие произошло
            //добавление нового слова
            if(requestCode == REQUEST_DICTIONARY_ADD_WORD){
                String studiedWord = data.getStringExtra(Card.STUDIED_KEY);
                String nativeWord = data.getStringExtra(Card.NATIVE_KEY);
                String transcription = data.getStringExtra(Card.TRANSCRIPTION_KEY);
                StateCard state = (StateCard) data.getSerializableExtra(Card.STATE_KEY);
                PartOfSpeech parts = (PartOfSpeech) data.getSerializableExtra(Card.PARTS_KEY);
                String themeWord = data.getStringExtra(Card.THEME_KEY);
                //добавляем в БД
                databaseAdapter.addWord(studiedWord, nativeWord, transcription, state, parts, themeWord);
                //проверяем темы
                DataAppHelper dataAppHelper = new DataAppHelper(getApplicationContext());
                dataAppHelper.verifyAddTheme(themeWord);
            }
            //изменение сушествующего
            if(requestCode == REQUEST_DICTIONARY_EDIT_WORD){
                long id = (long) data.getSerializableExtra(Card.ID_KEY);
                String studiedWord = data.getStringExtra(Card.STUDIED_KEY);
                String nativeWord = data.getStringExtra(Card.NATIVE_KEY);
                String transcription = data.getStringExtra(Card.TRANSCRIPTION_KEY);
                StateCard state = (StateCard) data.getSerializableExtra(Card.STATE_KEY);
                PartOfSpeech parts = (PartOfSpeech) data.getSerializableExtra(Card.PARTS_KEY);
                String themeWord = data.getStringExtra(Card.THEME_KEY);
                //добавляем в БД
                databaseAdapter.updateCard(new Card(id, studiedWord, nativeWord, state, LBApplication.PREF_COUNT_REPEAT, transcription, parts, themeWord));

                //проверяем темы
                DataAppHelper dataAppHelper = new DataAppHelper(getApplicationContext());
                dataAppHelper.verifyAddTheme(themeWord);
                dataAppHelper.verifyRemovedTheme(databaseAdapter);
        }
        }

    }
}
