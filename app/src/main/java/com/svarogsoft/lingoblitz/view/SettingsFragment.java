package com.svarogsoft.lingoblitz.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.svarogsoft.lingoblitz.R;
import com.svarogsoft.lingoblitz.card.StateCard;
import com.svarogsoft.lingoblitz.data.DatabaseAdapter;

public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

    SettingsActivity activity;

    public SettingsFragment(){}
    public SettingsFragment(SettingsActivity activity){this.activity = activity;}

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey){
        setPreferencesFromResource(R.xml.settings, rootKey);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(LBApplication.PREF_KEY_APP_LANGUAGE)){ //при изменении языка
            LBApplication.PREF_APP_LANGUAGE = sharedPreferences.getString(LBApplication.PREF_KEY_APP_LANGUAGE, LBApplication.PREF_APP_LANGUAGE);
        }
        if (key.equals(LBApplication.PREF_KEY_COUNT_CARD)){ //при изменении количества карт в колоде
            LBApplication.PREF_COUNT_CARD = Integer.parseInt(sharedPreferences.getString(LBApplication.PREF_KEY_COUNT_CARD, String.valueOf(LBApplication.PREF_COUNT_CARD)));
            //так как это меняется в настройках до инициализации колоды, никому ничего сообщать не надо
        }
        if (key.equals(LBApplication.PREF_KEY_COUNT_REPEAT)){ //при изменении счетчика повторений слова
            LBApplication.PREF_COUNT_REPEAT = Integer.parseInt(sharedPreferences.getString(LBApplication.PREF_KEY_COUNT_REPEAT, String.valueOf(LBApplication.PREF_COUNT_REPEAT)));
            DatabaseAdapter databaseAdapter = new DatabaseAdapter(activity.getApplicationContext());
            databaseAdapter.updateCountCardsByState(StateCard.READY, LBApplication.PREF_COUNT_REPEAT);
        }
        if (key.equals(LBApplication.PREF_KEY_COUNT_INCREASE)) { //при изменении инкремента повторений слова
            LBApplication.PREF_COUNT_INCREASE = Integer.parseInt(sharedPreferences.getString(LBApplication.PREF_KEY_COUNT_INCREASE, String.valueOf(LBApplication.PREF_COUNT_INCREASE)));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // регистрируем слушателя на изменение
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        // снимаем регистрацию слушателя на изменение
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }
}
