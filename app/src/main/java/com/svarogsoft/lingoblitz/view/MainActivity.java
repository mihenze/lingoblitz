package com.svarogsoft.lingoblitz.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.svarogsoft.lingoblitz.R;
import com.svarogsoft.lingoblitz.card.StudyMode;

import java.util.Locale;

//Основное окно приложения

public class MainActivity extends LocalActivity {

    private long backPressedTime; //время для выхода из приложения
    private Toast backToast; //всплывающее сообщение

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LBApplication.getInstance().initAppLanguage(this);
        setContentView(R.layout.activity_main);

        //убираем шторку
        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //кнопка для всех карточек
        Button buttonAllCards = (Button)findViewById(R.id.button_all_cards);
        buttonAllCards.setOnClickListener(v -> {
            try {
                Intent intent = new Intent(MainActivity.this, SelectionActivity.class);
                intent.putExtra(StudyMode.EXTRA_MODE, StudyMode.ALL);
                startActivity(intent); finish();
            } catch (Exception e){
                //TODO если не открылись и бог с ним
            }
        });

        //кнопка для частей речи
        Button buttonParts = (Button)findViewById(R.id.button_parts);
        buttonParts.setOnClickListener(v -> {
            try {
                Intent intent = new Intent(MainActivity.this, PartsActivity.class);
                startActivity(intent); finish();
            } catch (Exception e){
                // TODO если не открылись и бог с ним
            }
        });

        //кнопка для тем
        Button buttonTheme = (Button)findViewById(R.id.button_theme);
        buttonTheme.setOnClickListener(v -> {
            try {
                Intent intent = new Intent(MainActivity.this, ThemeActivity.class);
                startActivity(intent); finish();
            } catch (Exception e){
                //TODO если не открылись и бог с ним
            }
        });

        //кнопка словаря
        Button buttonDictionary = (Button)findViewById(R.id.button_dictionary);
        buttonDictionary.setOnClickListener(v -> {
            try {
                Intent intent = new Intent(MainActivity.this, MenuDictionaryActivity.class);
                startActivity(intent); finish();
            } catch (Exception e){
                //TODO если не открылись и бог с ним
            }
        });

        //кнопка настройки
        Button buttonSettings = (Button)findViewById(R.id.button_settings);
        buttonSettings.setOnClickListener(v -> {
            try {
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent); finish();
            } catch (Exception e){
                //TODO если не открылись и бог с ним
            }
        });
        /*TODO при первой загрузке, константы, при другой чтение из файла настроек
           не нравится что и тут  и в settings.xml прописываются стартовые значения
           Какие варианты решения?
           1) При первой загрузке стразу грузить экран настроек или ее облегченную версию
           2) Задавать константы тут, а при вызове настроек пихать их туда*/
        /**SharedPreferences sh = PreferenceManager.getDefaultSharedPreferences(this);
        SettingsActivity.initPrefVariable(sh);*/
    }

    /*@Override
    protected void attachBaseContext(Context newBase) {
        String s;
        if(LBApplication.PREF_APP_LANGUAGE.equals("English"))
            s = "en";
        else s = "ru";
        Locale languageType = new Locale(s);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, languageType));
    }*/

    //Системная кнопка назад
    @Override
    public void onBackPressed() {
        //для выхода из приложения
        if(backPressedTime+2000 > System.currentTimeMillis()){
            backToast.cancel(); //чтоб при закрытие пропадала и свплывающая подсказка
            super.onBackPressed();
            return;
        }
        else {
            //показываем всплывающую подсказку о выходе из приложения
            backToast = Toast.makeText(getBaseContext(), R.string.toast_exit_context, Toast.LENGTH_SHORT);
            backToast.show();
        }
        backPressedTime = System.currentTimeMillis();
    }
}
