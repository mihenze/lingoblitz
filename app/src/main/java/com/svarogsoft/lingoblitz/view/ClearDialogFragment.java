package com.svarogsoft.lingoblitz.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import com.svarogsoft.lingoblitz.R;
import com.svarogsoft.lingoblitz.dictionary.IClearDataBase;

public class ClearDialogFragment extends DialogFragment {

    private IClearDataBase clearDataBase;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        clearDataBase = (IClearDataBase) context;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        return builder
                .setTitle(R.string.toast_notice_clear_dialog_title)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(R.string.toast_notice_clear_dialog_message)
                .setPositiveButton(R.string.toast_notice_clear_dialog_OK, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        clearDataBase.clearDateBase();
                    }
                })
                .setNegativeButton(R.string.toast_notice_clear_dialog_cancel, null)
                .create();
    }
}
