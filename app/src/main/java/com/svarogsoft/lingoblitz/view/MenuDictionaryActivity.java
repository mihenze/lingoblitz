package com.svarogsoft.lingoblitz.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.svarogsoft.lingoblitz.R;
import com.svarogsoft.lingoblitz.data.DatabaseAdapter;
import com.svarogsoft.lingoblitz.dictionary.IClearDataBase;

public class MenuDictionaryActivity extends AppCompatActivity implements IClearDataBase {

    private Toast noticeToast; //всплывающее сообщение

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_dictionary);

        //убираем шторку
        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //кнопка назад
        Button buttonBack = (Button)findViewById(R.id.button_back);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //кнопка просмотра словаря
        Button buttonShow = (Button)findViewById(R.id.button_show);
        buttonShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuDictionaryActivity.this, DictionaryActivity.class);
                startActivity(intent); finish();
            }
        });

        //кнопка импорта
        Button buttonImport = (Button) findViewById(R.id.button_import);
        buttonImport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuDictionaryActivity.this, ImportActivity.class);
                startActivity(intent); finish();
            }
        });

        //Кнопка очистки БД
        Button buttonClear = (Button) findViewById(R.id.button_clear);
        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClearDialogFragment dialog = new ClearDialogFragment();
                dialog.show(getSupportFragmentManager(), "custom");
            }
        });


    }

    //Системная кнопка назад
    @Override
    public void onBackPressed() {
        try {
            Intent intent = new Intent(MenuDictionaryActivity.this, MainActivity.class);
            startActivity(intent); finish();
        } catch (Exception e){
            //если не открылись и бог с ним
        }
    }

    public void clearDateBase(){
        DatabaseAdapter databaseAdapter = new DatabaseAdapter(getApplicationContext());
        databaseAdapter.clearBD();
        noticeToast = Toast.makeText(getBaseContext(), R.string.toast_notice_menu_dictionary_clear, Toast.LENGTH_SHORT);
        noticeToast.show();
    }
}
