package com.svarogsoft.lingoblitz.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.svarogsoft.lingoblitz.R;
import com.svarogsoft.lingoblitz.card.ActionCardMode;
import com.svarogsoft.lingoblitz.card.Card;
import com.svarogsoft.lingoblitz.card.CardPresenter;
import com.svarogsoft.lingoblitz.card.PartOfSpeech;
import com.svarogsoft.lingoblitz.card.StudyMode;
import com.svarogsoft.lingoblitz.card.CardGestureListener;
import com.svarogsoft.lingoblitz.card.ICardView;


//Реализация MVP - View карточки

public class CardActivity extends LocalActivity implements ICardView {
    //переменные полученные при редактировании слова
    private String editStudiedWord;
    private String editNativeWord;
    //Параметр запроса
    private static final int REQUEST_EDIT_CARD = 1;

    private StudyMode studyMode; //режим изучения
    private PartOfSpeech typeWordMode; //для режима изучения по частям речи
    private String themeMode; //для режима изучения по темам

    //вьюхи карты
    TextView languageText;
    TextView transcriptionText;
    TextView partsOfSpeechText;
    //посредник между вьюхой карты и ее данными
    private CardPresenter presenter;
    //вьюха карты
    CardView cardView;

    private Toast noticeToast; //всплывающее сообщение

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);

        //убираем шторку
        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //устанавливаем режим изучения
        initStudyMode();

        //кнопка назад
        Button buttonBack = (Button)findViewById(R.id.button_back);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //кнопка редактировать
        Button buttonEdit = (Button)findViewById(R.id.button_edit);
        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEditQuickActivity();
            }

        });

        //получаем карту
        cardView = (CardView)findViewById(R.id.card_view);

        //находим TextView для слова и транскрипции
        languageText = (TextView)findViewById(R.id.text_word);
        transcriptionText = (TextView)findViewById(R.id.text_trans);

        //находим TextView для части речи
        partsOfSpeechText = (TextView)findViewById(R.id.text_parts_of_speech);

        //создаем Presenter FINAL
        switch (studyMode){
            case ALL: presenter = new CardPresenter(this, studyMode, getApplicationContext()); break;
            case PARTS: presenter = new CardPresenter(this, studyMode, typeWordMode, getApplicationContext()); break;
            case THEME: presenter = new CardPresenter(this, studyMode, themeMode, getApplicationContext()); break;
        }

        //События на карте:
        //1) мой слушатель
        CardGestureListener cardGestureListener = new CardGestureListener(presenter);
        //2) детектор жестов
        final GestureDetector gestureDetector = new GestureDetector(cardView.getContext(), cardGestureListener);
        //3) устанавливаем слушателя
        cardView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });

    }

    //запускаем окно быстрого редактирования
    private void onEditQuickActivity(){
        if(presenter.isCards()) {
            Intent intent = new Intent(CardActivity.this, EditCardActivity.class);
            intent.putExtra(Card.STUDIED_KEY, presenter.getStudiedWord());
            intent.putExtra(Card.NATIVE_KEY, presenter.getNativeWord());
            startActivityForResult(intent, REQUEST_EDIT_CARD);
        }

    }

    //результат редактирования данные после ввода данных в EditQuickActivity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if(requestCode == REQUEST_EDIT_CARD){
            if(resultCode == RESULT_OK){ //то мы должны уведомить presenter об этом
                editStudiedWord = data.getStringExtra(Card.STUDIED_KEY);
                editNativeWord = data.getStringExtra(Card.NATIVE_KEY);
                presenter.isCardEdit();
            }
        }

    }

    //Системная кнопка назад
    @Override
    public void onBackPressed() {
        presenter.close(); //уведомим посредника о выходе из изучения слов
        try {
            Intent intent = new Intent(CardActivity.this, SelectionActivity.class);
            intent.putExtra(StudyMode.EXTRA_MODE, studyMode); //передаем режим так как он нужен для правильного определения активностей
            startActivity(intent); finish();
        } catch (Exception e){
            //TODO если не открылись и бог с ним
        }
    }

    //TODO повторяется как и в СелектионАктивити установка значений режима
    private void initStudyMode(){
        //получаем тип изучения
        studyMode = (StudyMode)getIntent().getSerializableExtra(StudyMode.EXTRA_MODE);
        //если studyMode не ALL, то надо получить дополнительные фильтры и установить их
        if (studyMode == StudyMode.PARTS){
            typeWordMode = (PartOfSpeech)getIntent().getSerializableExtra(PartOfSpeech.PARTS_OF_SPEECH_MODE);
        }
        if (studyMode == StudyMode.THEME){
            themeMode = (String)getIntent().getSerializableExtra(ThemeActivity.THEME_MODE);
        }
    }

    public void showText(String word, String transcription, PartOfSpeech partOfSpeechWord){
        languageText.setText(word);
        transcriptionText.setText(transcription);
        partsOfSpeechText.setText(partOfSpeechWord.toString());
    }

    public void showToast(ActionCardMode mode){
        switch (mode) {
            case NEXT:
            case PREVIOUS:
            case TOUCH:
                if(noticeToast!= null)
                    noticeToast.cancel();
                break; //чтоб при следующем слове пропадала подсказка подсказка
            case PASS:
                noticeToast = Toast.makeText(getBaseContext(), R.string.toast_notice_pass_context, Toast.LENGTH_SHORT);
                noticeToast.show();
                break;
            case INCREASE:
                noticeToast = Toast.makeText(getBaseContext(), R.string.toast_notice_increase_context, Toast.LENGTH_SHORT);
                noticeToast.show();
                break;
        }

    }

    @Override
    public void showBackground(boolean isNormal) {
        if (isNormal){
            cardView.setCardBackgroundColor(getColor(R.color.eGray));
        } else {
            cardView.setCardBackgroundColor(getColor(R.color.eRed));
        }
    }

    public String getEditStudiedWord() {
        return editStudiedWord;
    }

    public String getEditNativeWord() {
        return editNativeWord;
    }
}
