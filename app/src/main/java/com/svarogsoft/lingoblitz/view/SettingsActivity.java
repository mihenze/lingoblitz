package com.svarogsoft.lingoblitz.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;

import android.os.Bundle;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.svarogsoft.lingoblitz.R;
import com.svarogsoft.lingoblitz.data.DatabaseAdapter;
import com.svarogsoft.lingoblitz.dictionary.IClearDataBase;

import java.util.Locale;

public class SettingsActivity extends AppCompatActivity implements IClearDataBase {

    private Toast noticeToast; //всплывающее сообщение

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);



        //убираем шторку
        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //кнопка назад
        Button buttonBack = (Button)findViewById(R.id.button_back);
        buttonBack.setOnClickListener(v -> onBackPressed());

        //TODO replace или add - в чем разница
        getSupportFragmentManager().beginTransaction().replace(R.id.main_layout, new SettingsFragment(this)).commit();

    }

    //Системная кнопка назад
    @Override
    public void onBackPressed() {
        try {
            Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
            startActivity(intent); finish();
        } catch (Exception e){
            //TODO если не открылись и бог с ним
        }
    }

    @Override
    public void clearDateBase() { //используем этот интерфейс и для сброса //TODO подумать о переименовании
        DatabaseAdapter databaseAdapter = new DatabaseAdapter(getApplicationContext());
        databaseAdapter.resetBD();
        noticeToast = Toast.makeText(getBaseContext(), R.string.toast_notice_settings_activity_reset, Toast.LENGTH_SHORT);
        noticeToast.show();
    }
}
