package com.svarogsoft.lingoblitz.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.svarogsoft.lingoblitz.R;
import com.svarogsoft.lingoblitz.card.Card;
import com.svarogsoft.lingoblitz.card.PartOfSpeech;
import com.svarogsoft.lingoblitz.card.StateCard;

import java.util.Arrays;


public class EditDictionaryActivity extends AppCompatActivity {
    //установщики параметров
    EditText studiedWord;
    EditText nativeWord;
    EditText transcription;
    EditText theme;
    Spinner state;
    Spinner parts;
    //идетнификатор слова в бд
    long id;
    //всплывающее сообщение
    private Toast noticeToast;
    //новая ли картв
    boolean isNewCard = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_dictionary);

        //убираем шторку
        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //получаем поля редактирования
        studiedWord = (EditText) findViewById(R.id.input_english);
        nativeWord = (EditText) findViewById(R.id.input_russian);
        transcription = (EditText) findViewById(R.id.input_transcription);
        theme = (EditText) findViewById(R.id.input_theme);
        state = (Spinner) findViewById(R.id.input_state);
        parts = (Spinner) findViewById(R.id.input_parts);

        //адаптер для состояний
        ArrayAdapter<StateCard> stateAdapter = new ArrayAdapter<>(this, R.layout.decorate_spinner_item_dictionary ,StateCard.values());
        stateAdapter.setDropDownViewResource(R.layout.decorate_spinner_dropdown_dictionary);
        state.setAdapter(stateAdapter);

        //адаптер для частей речи
        //TODO что делать если не хочу задавать часть речи ? - пока добавил none в enum partsofspeech
        ArrayAdapter<PartOfSpeech> partsAdapter = new ArrayAdapter<>(this, R.layout.decorate_spinner_item_dictionary , PartOfSpeech.values());
        partsAdapter.setDropDownViewResource(R.layout.decorate_spinner_dropdown_dictionary);
        parts.setAdapter(partsAdapter);

        //получаем переданные значения и выставляем их
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            isNewCard = false;
            id = extras.getLong(Card.ID_KEY);
            studiedWord.setText(extras.getString(Card.STUDIED_KEY));
            nativeWord.setText(extras.getString(Card.NATIVE_KEY));
            transcription.setText(extras.getString(Card.TRANSCRIPTION_KEY));
            theme.setText(extras.getString(Card.THEME_KEY));
            StateCard tempState = (StateCard) extras.getSerializable(Card.STATE_KEY);
            state.setSelection(tempState.ordinal());
            PartOfSpeech tempParts = (PartOfSpeech) extras.getSerializable(Card.PARTS_KEY);
            parts.setSelection(tempParts.ordinal());

        }

        //кнопка назад
        Button buttonBack = (Button) findViewById(R.id.button_back);
        //выставляем слушателя на кнопку назад
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(noticeToast!= null)
                    noticeToast.cancel();

                setResult(RESULT_CANCELED);
                finish();
            }
        });

        //кнопка ок
        Button buttonOk = (Button) findViewById(R.id.button_ok_input);
        //выставляем слушателя на кнопку OK
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //проверяем не пусты ли обязательные поля
                if (studiedWord.getText().toString().equals("") || nativeWord.getText().toString().equals("")){
                    //выводим сообщение, что обязательные поля должны быть заполнены
                    noticeToast = Toast.makeText(getBaseContext(), R.string.toast_notice_input_dictionary, Toast.LENGTH_SHORT);
                    noticeToast.show();

                } else if (!isCheckedTheme(theme.getText().toString())) { //если тема не правильно заполнена
                    //выводим сообщение о правильном заполнении полей темы
                    noticeToast = Toast.makeText(getBaseContext(), R.string.toast_notice_input_theme_dictionary, Toast.LENGTH_SHORT);
                    noticeToast.show();

                } else {
                    if(noticeToast!= null)
                        noticeToast.cancel();
                    Intent data = new Intent();
                    if(!isNewCard) {
                        data.putExtra(Card.ID_KEY, id);
                    }
                    data.putExtra(Card.STUDIED_KEY, studiedWord.getText().toString());
                    data.putExtra(Card.NATIVE_KEY, nativeWord.getText().toString());

                    //TODO НУжны ли NULL или сделать в БД по умолчанию пусто ""?
                    //сейчас для восстановления нулов сделана эта строка
                    String temp_transcription = transcription.getText().toString();
                    if(temp_transcription.equals(""))
                        temp_transcription = null;
                    data.putExtra(Card.TRANSCRIPTION_KEY, temp_transcription);

                    data.putExtra(Card.STATE_KEY, StateCard.values()[state.getSelectedItemPosition()]);
                    data.putExtra(Card.PARTS_KEY, PartOfSpeech.values()[parts.getSelectedItemPosition()]);
                    //TODO НУжны ли NULL или сделать в БД по умолчанию пусто ""?
                    //сейчас для восстановления нулов сделана эта строка
                    String temp_theme = theme.getText().toString();
                    if (temp_theme.equals(""))
                        temp_theme = null;
                    data.putExtra(Card.THEME_KEY, temp_theme);

                    setResult(RESULT_OK, data);
                    finish();
                }
            }
        });


    }
    //проверка на неверный ввод темы
    boolean isCheckedTheme(String theme){
        if (theme.equals("")) return true;

        String[] arrayTheme = theme.split(", ");
        //TODO и тут API 24
        return Arrays.stream(arrayTheme).allMatch(s -> !s.contains(","));
    }
}
