package com.svarogsoft.lingoblitz.view;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.svarogsoft.lingoblitz.R;

import java.util.List;

public class ThemeAdapter extends RecyclerView.Adapter<ThemeAdapter.ViewHolder> {
    private LayoutInflater inflater;
    private List<String> captions;
    private OnCaptionClickListener onCaptionClickListener; //слушатель кликов

    public ThemeAdapter(Context context, List<String> captions, OnCaptionClickListener onCaptionClickListener){

        this.captions = captions;
        this.inflater = LayoutInflater.from(context);
        this.onCaptionClickListener = onCaptionClickListener;
    }

    //возвращает объект ViewHolder, который будет хранить данные по одному объекту
    @Override
    public  ThemeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = inflater.inflate(R.layout.decorate_theme, parent, false);
        return new ViewHolder(view);
    }

    //выполняет привязку объекта ViewHolder к объекту String по определенной позиции.
    @Override
    public void onBindViewHolder(ThemeAdapter.ViewHolder holder, int position) {
        String caption = captions.get(position);
        holder.captionView.setText(caption);
    }

    //возвращает количество объектов в списке
    @Override
    public int getItemCount() {
        return captions.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView captionView;
        public ViewHolder(View view) {
            super(view);
            captionView = (TextView) view.findViewById(R.id.caption);
            //обработчик клика
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String caption = captions.get(getLayoutPosition());
                    onCaptionClickListener.onCaptionClick(caption);
                }
            });
        }
    }

    public interface OnCaptionClickListener {
        void onCaptionClick(String caption);
    }
}
