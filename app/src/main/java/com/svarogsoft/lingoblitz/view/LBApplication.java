package com.svarogsoft.lingoblitz.view;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

import com.svarogsoft.lingoblitz.R;

import java.util.Locale;

public class LBApplication extends Application {
    //константы ключей для полей preference
    public static String PREF_KEY_APP_LANGUAGE;
    public static String PREF_KEY_COUNT_CARD;
    public static String PREF_KEY_COUNT_REPEAT;
    public static String PREF_KEY_COUNT_INCREASE;
    public static String PREF_KEY_DIALOG_FRAGMENT;

    //сами данные
    public static String PREF_APP_LANGUAGE = "English";
    public static int PREF_COUNT_REPEAT = 10;
    public static int PREF_COUNT_INCREASE = 5;
    public static int PREF_COUNT_CARD = 5;

    private static LBApplication applicationInstance;

    public static synchronized LBApplication getInstance() {
        return applicationInstance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        PREF_KEY_APP_LANGUAGE = getResources().getString(R.string.caption_list_app_language);
        PREF_KEY_COUNT_CARD = getResources().getString(R.string.caption_card_count);
        PREF_KEY_COUNT_REPEAT = getResources().getString(R.string.caption_repeat_count);
        PREF_KEY_COUNT_INCREASE = getResources().getString(R.string.caption_increase_count);
        PREF_KEY_DIALOG_FRAGMENT = getResources().getString(R.string.caption_reset_progress);
        initAppLanguage(this);
        applicationInstance = this;
    }

    public void initAppLanguage(Context context) {
        SharedPreferences sh = PreferenceManager.getDefaultSharedPreferences(context);
        PREF_APP_LANGUAGE = sh.getString(PREF_KEY_APP_LANGUAGE, PREF_APP_LANGUAGE);
        PREF_COUNT_CARD = Integer.parseInt(sh.getString(PREF_KEY_COUNT_CARD, String.valueOf(PREF_COUNT_CARD)));
        PREF_COUNT_REPEAT = Integer.parseInt(sh.getString(PREF_KEY_COUNT_REPEAT, String.valueOf(PREF_COUNT_REPEAT)));
        PREF_COUNT_INCREASE = Integer.parseInt(sh.getString(PREF_KEY_COUNT_INCREASE, String.valueOf(PREF_COUNT_INCREASE)));
        if (PREF_APP_LANGUAGE.equals("English"))
            LocaleUtils.initialize(context, "en");
        else LocaleUtils.initialize(context, "ru");

    }


}
