package com.svarogsoft.lingoblitz.view;

import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.preference.DialogPreference;

import com.svarogsoft.lingoblitz.R;
import com.svarogsoft.lingoblitz.dictionary.IClearDataBase;

public class ResetDialogPreference extends DialogPreference {

    IClearDataBase clearDataBase;

    public ResetDialogPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        clearDataBase = (IClearDataBase) context;
    }

    @Override
    protected void onClick() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle(R.string.pref_title_reset_progress);
        dialog.setIcon(android.R.drawable.ic_dialog_alert);
        dialog.setMessage(R.string.pref_dialog_reset_progress);
        dialog.setCancelable(true);
        dialog.setPositiveButton(R.string.pref_dialog_positive_reset_progress, (dialog1, which) -> {
            //reset database
            clearDataBase.clearDateBase();
        });

        dialog.setNegativeButton(R.string.pref_dialog_negative_reset_progress, (dialog12, which) -> dialog12.cancel());

        AlertDialog al = dialog.create();
        al.show();
    }
}
