package com.svarogsoft.lingoblitz.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.svarogsoft.lingoblitz.R;
import com.svarogsoft.lingoblitz.card.Card;

public class EditCardActivity extends AppCompatActivity {

    EditText editStudiedWord;
    EditText editNativeWord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_card);

        //убираем шторку
        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //получает поля редактирования
        editStudiedWord = (EditText) findViewById(R.id.edit_english);
        editNativeWord = (EditText) findViewById(R.id.edit_russian);

        //получаем переданные значения и выставляем их
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
           editStudiedWord.setText(extras.getString(Card.STUDIED_KEY));
           editNativeWord.setText(extras.getString(Card.NATIVE_KEY));
        }

        //получаем кнопки
        final Button buttonBack = (Button) findViewById(R.id.button_back);
        Button buttonOk = (Button) findViewById(R.id.button_ok_edit);

        //выставляем слушателей
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editStudiedWord.getText().toString().equals("") || editNativeWord.getText().toString().equals("")){
                    setResult(RESULT_CANCELED);
                    finish();
                } else {
                    Intent data = new Intent();
                    data.putExtra(Card.STUDIED_KEY, editStudiedWord.getText().toString());
                    data.putExtra(Card.NATIVE_KEY, editNativeWord.getText().toString());
                    setResult(RESULT_OK, data);
                    finish();
                }
            }
        });
    }

}
