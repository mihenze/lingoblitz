package com.svarogsoft.lingoblitz.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.svarogsoft.lingoblitz.R;
import com.svarogsoft.lingoblitz.card.PartOfSpeech;
import com.svarogsoft.lingoblitz.card.StudyMode;

public class PartsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parts);

        //убираем шторку
        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //кнопка назад
        Button buttonBack = (Button)findViewById(R.id.button_back);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //кнопка существительных
        Button buttonNoun = (Button)findViewById(R.id.button_noun);
        buttonNoun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PartsActivity.this, SelectionActivity.class);
                intent.putExtra(StudyMode.EXTRA_MODE, StudyMode.PARTS);
                intent.putExtra(PartOfSpeech.PARTS_OF_SPEECH_MODE, PartOfSpeech.NOUN);
                startActivity(intent); finish();
            }
        });

        //кнопка прилагательных
        Button buttonAdjective = (Button)findViewById(R.id.button_adjective);
        buttonAdjective.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PartsActivity.this, SelectionActivity.class);
                intent.putExtra(StudyMode.EXTRA_MODE, StudyMode.PARTS);
                intent.putExtra(PartOfSpeech.PARTS_OF_SPEECH_MODE, PartOfSpeech.ADJECTIVE);
                startActivity(intent); finish();
            }
        });

        //кнопка глаголов
        Button buttonVerb= (Button)findViewById(R.id.button_verb);
        buttonVerb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PartsActivity.this, SelectionActivity.class);
                intent.putExtra(StudyMode.EXTRA_MODE, StudyMode.PARTS);
                intent.putExtra(PartOfSpeech.PARTS_OF_SPEECH_MODE, PartOfSpeech.VERB);
                startActivity(intent); finish();
            }
        });
    }

    //Системная кнопка назад
    @Override
    public void onBackPressed() {
        try {
            Intent intent = new Intent(PartsActivity.this, MainActivity.class);
            startActivity(intent); finish();
        } catch (Exception e){
            //если не открылись и бог с ним
        }
    }
}
