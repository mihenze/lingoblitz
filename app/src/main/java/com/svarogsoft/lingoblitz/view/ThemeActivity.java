package com.svarogsoft.lingoblitz.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.svarogsoft.lingoblitz.R;
import com.svarogsoft.lingoblitz.data.DataAppHelper;
import com.svarogsoft.lingoblitz.card.StudyMode;

import java.util.ArrayList;
import java.util.List;

public class ThemeActivity extends AppCompatActivity {

    List<String> captions = new ArrayList<>();

    public static final String THEME_MODE = "THEME_MODE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theme);
        //убираем шторку
        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //кнопка назад
        Button buttonBack = (Button)findViewById(R.id.button_back);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //определение названий тем
        setInitialData();

        //Список тем
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list_theme);
        //Обработка клика по выбранной теме
        ThemeAdapter.OnCaptionClickListener onCaptionClickListener = new ThemeAdapter.OnCaptionClickListener() {
            @Override
            public void onCaptionClick(String caption) {
                Intent intent = new Intent(ThemeActivity.this, SelectionActivity.class);
                intent.putExtra(StudyMode.EXTRA_MODE, StudyMode.THEME);
                intent.putExtra(ThemeActivity.THEME_MODE, caption);
                startActivity(intent); finish();
            }
        };
        //Установка адптера в view
        ThemeAdapter themeAdapter = new ThemeAdapter(this, captions, onCaptionClickListener);
        recyclerView.setAdapter(themeAdapter);
    }

    private void setInitialData(){
        //получаем названия тем из файла
        DataAppHelper dataAppHelper = new DataAppHelper(getApplicationContext());
        captions = dataAppHelper.getCaptionsFromJSON();
    }

    //Системная кнопка назад
    @Override
    public void onBackPressed() {
        try {
            Intent intent = new Intent(ThemeActivity.this, MainActivity.class);
            startActivity(intent); finish();
        } catch (Exception e){
            //TODO если не открылись и бог с ним
        }
    }
}
